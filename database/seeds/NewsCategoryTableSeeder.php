<?php

use App\Models\NewsCategory;
use Illuminate\Database\Seeder;

class NewsCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Artículos" => "articulos",
            "Eventos" => "eventos",
            "Entrevistas" => "entrevistas"
        ];
    
        foreach ($categories as $category => $slug){
            DB::table('news_category')->insert([
                'name' => $category,
                'slug' => $slug,
                'visible' => 1,
            ]);
        }
    }
}
