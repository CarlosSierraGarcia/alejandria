<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable([
          'role', 'sector', 'phase', 'news_category'
        ]);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(SectorTableSeeder::class);
        $this->call(PhaseTableSeeder::class);
        $this->call(NewsCategoryTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(InvestmentTableSeeder::class);
        $this->call(ContactTableSeeder::class);
    }
    
    /**
     * This method truncate the tables with specific values
     */
    protected function truncateTable(array $tables){
      DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
      foreach ($tables as $table){
        DB::table($table)->truncate();
      }
      DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
