<?php

use App\Models\Project;
use App\Models\Sector;
use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Project::class, 30)->create();

        $sectors = Sector::all();
        Project::all()->each(function ($project) use ($sectors) {
            $project->sectors()->attach(
                $sectors->random(rand(1, 14))->pluck('id')->toArray()
            );
        });
    }
}
