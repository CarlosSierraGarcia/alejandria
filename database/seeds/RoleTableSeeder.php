<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
          ["ADMIN" ,"administrators", "Adminitrador"],
          ["INVESTOR", "investors", "Inversor"],
          ["ENTREPRENEUR", "entrepreneurs", "Emprendedor"]
        ];
        foreach ($roles as $role){
          DB::table('role')->insert([
            'name' => $role[0],
            'slug' => $role[1],
            'text' => $role[2]
          ]);
        }
    }
}
