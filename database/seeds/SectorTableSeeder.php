<?php

use Illuminate\Database\Seeder;

class SectorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectors = [
            ["Agroalimentación", "agroalimentacion.jpg", "agroalimentacion"],
            ["Apps & Mobile", "app_mobile.jpg", "apps-mobile"],
            ["Comercio y Distribución", "comercio_distribucion.jpg", "comercio-distribucion"],
            ["Construcción e Inmobiliaria", "construccion_inmobiliaria.jpg", "construccion-inmobiliaria"],
            ["Ecología y Energía", "ecologia_energia.jpg", "ecologia-energia"],
            ["Educación y Cultura", "educacion_cultura.jpg", "educacion-cultura"],
            ["Exportación e Importación", "exportacion_importacion.jpg", "exportacion-importacion"],
            ["Industria", "industria.jpg", "industria"],
            ["Hostelería", "hosteleria.jpg", "hosteleria"],
            ["Ocio y Espectáculos", "ocio_espectaculos.jpg", "ocio-espectaculos"],
            ["Marketing y Comunicación", "marketing_comunicacion.jpg", "marketing-comunicacion"],
            ["Salud y Belleza", "saluz_belleza.jpg", "saluz-belleza"],
            ["Sanidad", "sanidad.jpg", "sanidad"],
            ["Software" ,"software.jpg", "software"]
        ];
        foreach ($sectors as $sector){
            DB::table('sector')->insert([
                'name' => $sector[0],
                'image' => $sector[1],
                'slug' => $sector[2],
                'visible' => 1
            ]);
        }
    }
}
