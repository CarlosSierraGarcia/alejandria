<?php
    
    use App\Models\Investment;
    use Illuminate\Database\Seeder;

class InvestmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Investment::class, 50)->create();
    }
}
