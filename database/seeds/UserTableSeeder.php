<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(User::class, 50)->create();
  
      $roles = Role::all();
      User::all()->each(function ($user) use ($roles) {
        $user->roles()->attach(
          $roles->random(rand(1, 3))->pluck('id')->toArray()
        );
      });
    }
}
