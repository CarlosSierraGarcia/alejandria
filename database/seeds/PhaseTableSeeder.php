<?php

use Illuminate\Database\Seeder;

class PhaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phases = [
            "Fase 1: Aprobación" => "approval",
            "Fase 2: Financiación" => "funding",
            "Fase 3: Finalización" => "completed"
        ];
        
        foreach ($phases as $phase => $slug){
          DB::table('phase')->insert([
            'name' => $phase,
            'slug' => $slug
          ]);
        }
    }
}
