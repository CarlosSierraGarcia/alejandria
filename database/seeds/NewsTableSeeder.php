<?php

use App\Models\News;
use App\Models\NewsCategory;
use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(News::class, 20)->create();
    }
}
