<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('user_id', 'fk_user_news')->references('id')->on('user')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('user_id');
            $table->foreign('news_category_id', 'fk_news_category')->references('id')->on('news_category')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('news_category_id');
            $table->string('title');
            $table->string('image');
            $table->boolean('visible');
            $table->longText('body');
            $table->longText('author');
            $table->string('slug', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
