<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectorProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_sector', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sector_id');
            $table->foreign('sector_id', 'fk_sector_project')->references('id')->on('sector')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('project_id');
            $table->foreign('project_id', 'fk_project_sector')->references('id')->on('project')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sector_project');
    }
}
