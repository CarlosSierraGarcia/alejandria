<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('lastname', 100);
            $table->date('birthday');
            $table->string('dni', 20);
            $table->string('phone', 50);
            $table->string('address', 100);
            $table->string('city', 100);
            $table->string('country', 100);
            $table->string('postcode', 25);
            $table->string('email', 100);
            $table->string('password', 100);
            $table->string('image', 100);
            $table->dateTime('last_access');
            $table->integer('num_access');
            $table->longText('comments');
            $table->boolean('blocked');
            $table->boolean('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
