<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'fk_investment_user')->references('id')->on('user')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('project_id');
            $table->foreign('project_id', 'fk_investment_project')->references('id')->on('project')->onDelete('restrict')->onUpdate('restrict');
            $table->string('owner', 150);
            $table->decimal('investment_amount', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment');
    }
}
