<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'fk_project_user')->references('id')->on('user')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('phase_id');
            $table->foreign('phase_id', 'fk_project_phase')->references('id')->on('phase')->onDelete('restrict')->onUpdate('restrict');
            $table->string('image', 100);
            $table->string('title', 150);
            $table->longText('description');
            $table->string('slug', 150);
            $table->dateTime('publish_date');
            $table->boolean('visible');
            $table->decimal('need_amount', 10);
            $table->integer('visited');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
