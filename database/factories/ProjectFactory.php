<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\News;
    use App\Models\Phase;
    use App\Models\Project;
    use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        "user_id" => factory(User::class)->create()->id,
        "phase_id" => $faker->numberBetween(1, 3),
        "image" => "default.jpg",
        "title" => $faker->realText(25),
        "description" => $faker->realText(),
        "slug" => $faker->slug,
        "publish_date" => now(),
        "visible" => $faker->boolean,
        "need_amount" => $faker->randomNumber(6),
        "visited" => $faker->randomNumber(2),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
