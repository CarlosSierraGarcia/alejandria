<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'birthday' => $faker->date(),
        'dni' => '50236892Q',
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'city' => $faker->city,
        'country' => $faker->country,
        'postcode' => $faker->postcode,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123'),
        'image' => 'default.jpg',
        'last_access' => $faker->dateTime,
        'num_access' => $faker->numberBetween(1, 20),
        'comments' => $faker->text(100),
        'blocked' => $faker->boolean,
        'deleted' => $faker->boolean,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
