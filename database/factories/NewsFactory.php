<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\News;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    return [
        "news_category_id" => $faker->numberBetween(1, 3),
        "user_id" => factory(User::class)->create()->id,
        "title" => $faker->realText(25),
        "body" => $faker->realText(),
        "author" => $faker->name,
        "slug" => $faker->slug,
        "visible" => $faker->boolean,
        "image" => 'default.jpg',
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
