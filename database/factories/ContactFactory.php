<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models;
use App\Models\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'email' => $faker->email,
        'subject' => $faker->text(15),
        'message' => $faker->paragraph,
        'answered' => $faker->boolean,
        'created_at' => $faker->dateTime
    ];
});
