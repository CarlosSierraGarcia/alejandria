<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Investment;
use App\Models\Project;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Investment::class, function (Faker $faker) {
    return [
        "user_id" => factory(User::class)->create()->id,
        "project_id" => factory(Project::class)->create()->id,
        "owner" => $faker->name,
        "investment_amount" => $faker->randomNumber(4),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
