<?php

/*
|--------------------------------------------------------------------------
| Web Routes Front
|--------------------------------------------------------------------------
*/

/** Home Routes */
Route::get('', 'InicioController@index')->name('inicio');

/** Login and register Routes */
Route::post('login', 'UserController@login')->name('user_login');

/** Blog Routes */
Route::get('blog/{page?}', 'BlogController@index')->name('blog');
Route::get('blog/{slug?}', 'BlogController@index')->name('blog_category');
Route::get('blog/news/{slug}', 'BlogController@interna')->name('blog_interna');

/** About us Routes */
Route::get('about-us', 'AboutUsController@index')->name('about_us');

/** Contact Routes */
Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact/send', 'ContactController@send')->name('contact_send');

/** Projects Routes */
Route::get('projects/{page?}', 'ProjectController@index')->name('projects');
Route::get('project/{slug}', 'ProjectController@interna')->name('project_interna');
Route::post('project/search', 'ProjectController@search')->name('search_project');
Route::post('project/investment/{id}', 'ProjectController@investment')->name('project_investment');

/** Register and Create Routes */
Route::get('register', 'UserController@register')->name('register');
Route::post('create', 'UserController@create')->name('create');

/** Profile User Routes */
Route::group(['prefix' => 'profile', 'middleware' => ['profile']], function(){
    Route::get('', 'UserController@profile')->name('profile');
    Route::post('edit/{id}', 'UserController@edit')->name('edit');
    Route::get('login_out', 'UserController@login_out')->name('login_out');
    Route::get('my-projects', 'UserController@my_projects')->name('my_projects');
    Route::get('my-projects/create', 'UserController@my_projects_create')->name('my_projects_create');
    Route::post('my-projects/store', 'UserController@my_projects_store')->name('my_projects_store');
    Route::get('my-investments', 'UserController@my_investments')->name('my_investments');
});

/*
|--------------------------------------------------------------------------
| Web Routes BackOffice
|--------------------------------------------------------------------------
*/

Route::get('admin/login', 'Admin\LoginController@index')->name('login');
Route::post('admin/login', 'Admin\LoginController@login')->name('login_post');
Route::get('admin/logout', 'Admin\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function(){
  Route::get('', 'DashboardController@index')->name('opening');
  
  /** User routes */
  Route::get('users/create', 'UserController@create')->name('user_create');
  Route::post('users/store', 'UserController@store')->name('user_store');
  Route::get('users/{type}', 'UserController@index')->name('users_list');
  Route::get('users/show/{id}', 'UserController@show')->name('user_show');
  Route::get('users/edit/{id}', 'UserController@edit')->name('user_edit');
  Route::put('users/edit/{id}/update', 'UserController@update')->name('user_update');
  Route::get('users/delete/{id}', 'UserController@delete')->name('user_delete');
  Route::get('users/block/{id}/{value}', 'UserController@block')->name('user_block');
  
  /** Lockscreen routes */
  Route::get('lockscreen/{id}', 'LockscreenController@index')->name('lockscreen');
  Route::post('ajaxLockscreen', 'LockscreenController@lockSession')->name('locksession');
  Route::post('ajaxunLockscreen', 'LockscreenController@unlockSession')->name('unlocksession');
  
  /** Project routes */
  Route::get('projects/create', 'ProjectController@create')->name('project_create');
  Route::post('project/store', 'ProjectController@store')->name('project_store');
  Route::get('project/accepted/{id}', 'ProjectController@accepted')->name('project_accepted');
  Route::get('projects/{type}', 'ProjectController@index')->name('project_list');
  Route::get('project/show/{id}', 'ProjectController@show')->name('project_show');
  Route::put('project/edit/{id}/update', 'ProjectController@update')->name('project_update');
  Route::get('project/edit/{id}', 'ProjectController@edit')->name('project_edit');
  Route::get('project/block/{id}/{value}', 'ProjectController@block')->name('project_block');
  
  /** Sectors routes */
  Route::get('sectors/create', 'SectorController@create')->name('sector_create');
  Route::post('sectors/store', 'SectorController@store')->name('sector_store');
  Route::get('sectors/list', 'SectorController@index')->name('sector_list');
  Route::put('sector/edit/{id}/update', 'SectorController@update')->name('sector_update');
  Route::get('sector/edit/{id}', 'SectorController@edit')->name('sector_edit');
  Route::get('sector/block/{id}/{value}', 'SectorController@block')->name('sector_block');
  
  /** Category routes */
  Route::get('categories/create', 'NewsCategoryController@create')->name('category_create');
  Route::post('categories/store', 'NewsCategoryController@store')->name('category_store');
  Route::get('categories/list', 'NewsCategoryController@index')->name('category_list');
  Route::put('category/edit/{id}/update', 'NewsCategoryController@update')->name('category_update');
  Route::get('category/edit/{id}', 'NewsCategoryController@edit')->name('category_edit');
  Route::get('category/block/{id}/{value}', 'NewsCategoryController@block')->name('category_block');
  
  /** News routes */
  Route::get('news/create', 'NewsController@create')->name('news_create');
  Route::post('news/store', 'NewsController@store')->name('news_store');
  Route::get('news/list', 'NewsController@index')->name('news_list');
  Route::get('news/show/{id}', 'NewsController@show')->name('news_show');
  Route::put('news/edit/{id}/update', 'NewsController@update')->name('news_update');
  Route::get('news/edit/{id}', 'NewsController@edit')->name('news_edit');
  Route::get('news/block/{id}/{value}', 'NewsController@block')->name('news_block');
  
  /** Investment routes */
  Route::get('investment/list', 'investmentController@index')->name('investment_list');
  
  /** Contact routes */
  Route::get('contact/list/{answered}', 'ContactController@index')->name('contact_list');
  Route::get('contact/show/{id}', 'ContactController@show')->name('contact_show');
  Route::get('contact/answered/{id}/{value}', 'ContactController@answered')->name('contact_answered');
});
