<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->isAdmin()){
            return $next($request);
        } else {
            $request->session()->invalidate();
            return redirect('/admin/login')->withErrors(['error' => 'Permiso denegado.']);
        }

    }

    private function isAdmin(){
        $result = false;
        foreach (session()->get('role') as $role){
            if ($role['name'] == 'ADMIN'){
                $result = true;
            }
        }

        return $result;
    }
}
