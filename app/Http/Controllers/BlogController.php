<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = '')
    {
        if (!empty($slug)){
            $blog = DB::table('news')
                ->select('news.*')
                ->join('news_category', 'news.news_category_id', '=', 'news_category.id')
                ->Where('news.visible', '=', true)
                ->Where('news_category.visible', '=', true)
                ->Where('news_category.slug', '=', $slug)
                ->orderBy('created_at', 'desc')
                ->get();
        } else {
            $blog = DB::table('news')
                ->select('news.*')
                ->join('news_category', 'news.news_category_id', '=', 'news_category.id')
                ->Where('news.visible', '=', true)
                ->Where('news_category.visible', '=', true)
                ->orderBy('created_at', 'desc')
                ->get();
        }
        
        $categories = DB::table('news_category')
            ->Where('visible', '=', true)
            ->orderBy('name', 'desc')
            ->get();

        return view("blog", compact(['categories', 'blog']));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function interna($slug)
    {
        $news = DB::table('news')
            ->select('news.*')
            ->join('news_category', 'news.news_category_id', '=', 'news_category.id')
            ->Where('news.visible', '=', true)
            ->Where('news.slug', '=', $slug)
            ->Where('news_category.visible', '=', true)
            ->first();
        
        
        return view("blog_interna", compact('news'));
    }
    
    public function getCategories(){
        return DB::table('news_category')
            ->where('visible', '=', true)
            ->get();
    }
}
