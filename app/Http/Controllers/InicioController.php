<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = DB::table('project')
            ->where('phase_id', '=', 2)
            ->Where('visible', '=', true)
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();
        
        $blog = DB::table('news')
            ->where('visible', '=', true)
            ->limit(4)
            ->get();

        return view("inicio", compact(['projects', 'blog']));
    }
}
