<?php

namespace App\Http\Controllers\Admin;

use App\Models\Investment;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Project;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic;
use mysql_xdevapi\Table;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('admin.news.list', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = NewsCategory::all();
    
        return view('admin.news.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg|max:2048',
            'title' => 'required',
            'body' => 'required',
            'author' => 'required',
            'category' => 'required'
            
        ]);
    
        $news = new News([
            'title' => addslashes($request->get('title')),
            'body' => addslashes($request->get('body')),
            'author' => $request->get('author'),
            'image' => 'default.jpg',
            'visible' => 0
        ]);
        
        $news->news_category_id = $request->get('category');
        $news->user_id = Session::get('id');
        $news->slug = str_slug($request->get('title'));
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(282, 200);
            $img->save('img/news/'.$filename);
            $news->image = $filename;
        }
    
        $news->save();
        return redirect()->route('news_show', ['id' => encrypt($news->id)])->with('success', 'Noticía guardada correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find(decrypt($id));
        $categories = NewsCategory::all();
        
        return view('admin.news.show', compact(['news', 'categories']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find(decrypt($id));
        $categories = NewsCategory::all();
    
        return view('admin.news.edit', compact(['news', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::find(decrypt($id));
    
        $request->validate([
            'title' => 'required',
            'author' => 'required',
            'body' => 'required',
            'category' => 'required'
        ]);
    
        $news->title = addslashes(request('title'));
        $news->body = addslashes(request('body'));
        $news->news_category_id = request('category');
        $news->slug = str_slug(request('title'));
        $news->user_id = Session::get('id');
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(282, 200);
            $img->save('img/news/'.$filename);
            $news->image = $filename;
        }
    
        $news->save();
        return redirect()->route('news_show', ['id' => $id])->with('success','Registro actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function block($id, $value){
        News::find(decrypt($id))->update(['visible' => $value]);
        $message = $value ? 'Noticía bloqueado descorrectamente.' : 'Noticía bloqueado correctamente.';
        return redirect()->back()->with('success',$message);
    }
}
