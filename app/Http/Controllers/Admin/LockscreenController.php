<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LockscreenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      $user = User::find(decrypt($id));
      return view("theme.lte.lockscreen", compact('user'));
    }
    
    public function lockSession(Request $request){
        $visible = $request->get('visible');
        Session::put('lockscreen', $visible);
        return response()->json(['success'=>'ok']);
    }
    
    public function unlockSession(Request $request){
        if(Hash::check($request->input('password'), $request->user()->password)) {
            Session::put('lockscreen', false);
            return response()->json(['success'=>'ok']);
        } else {
            return response()->json(['error'=>'error']);
        }
    }
}
