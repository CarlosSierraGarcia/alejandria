<?php

namespace App\Http\Controllers\Admin;

use App\Models\Investment;
use App\Models\Project;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic;
use mysql_xdevapi\Table;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard.inicio');
    }
    
    public function getAllInvestments(){
        return DB::table('investment')->sum("investment_amount");
    }
    
    public function getAllProjects(){
        return DB::table('investment')->count();
    }
    
    public function getAllEntrepreneurs(){
        return DB::table('user')->join('role_user', 'role_user.id', '=', 'user.id')->where('role_id', '=', 3)->count();
    }
    
    public function getAllInvestors(){
        return DB::table('user')->join('role_user', 'role_user.id', '=', 'user.id')->where('role_id', '=', 2)->count();
    }
    
    public function getAllAdmins(){
        return DB::table('user')->join('role_user', 'role_user.id', '=', 'user.id')->where('role_id', '=', 1)->count();
    }
    
    public function getProjectsByMonth(){
        $resultado = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $projects = DB::table('project')->whereYear('created_at', '=', NOW())->get();
        foreach ($projects as $project){
            $month = date("n",strtotime($project->created_at));
            $resultado[$month - 1]++;
        }
        
        return $resultado;
    }
    
    public function getInvesmentsByMonth(){
        $resultado = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $investments = DB::table('investment')->whereYear('created_at', '=', NOW())->get();
        foreach ($investments as $investment){
            $month = date("n",strtotime($investment->created_at));
            $resultado[$month - 1]++;
        }
        
        return $resultado;
    }
}
