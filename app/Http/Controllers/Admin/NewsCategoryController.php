<?php

namespace App\Http\Controllers\Admin;

use App\Models\Investment;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Project;
use App\Models\Sector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic;
use mysql_xdevapi\Table;

class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = NewsCategory::all();
        return view('admin.category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
    
        $category = new NewsCategory([
            'name' => $request->get('name')
        ]);
    
        $category->slug = str_slug($request->get('name'));
        $category->visible = 0;
    
        $category->save();
        return redirect()->route('category_list')->with('success', 'Categoría guardada correctamente!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = NewsCategory::find(decrypt($id));
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = NewsCategory::find(decrypt($id));
    
        $request->validate([
            'name' => 'required'
        ]);
    
        $category->name = request('name');
        $category->slug = str_slug(request('name'));
    
        $category->save();
        return redirect()->route('category_edit', ['id' => $id])->with('success','Registro actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function block($id, $value){
        NewsCategory::find(decrypt($id))->update(['visible' => $value]);
        $message = $value ? 'Categoría desbloqueado correctamente.' : 'Categoría bloqueado correctamente.';
        return redirect()->back()->with('success',$message);
    }
}
