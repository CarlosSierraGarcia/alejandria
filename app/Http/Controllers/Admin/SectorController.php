<?php

namespace App\Http\Controllers\Admin;

use App\Models\Investment;
use App\Models\Project;
use App\Models\Sector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic;
use mysql_xdevapi\Table;

class SectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectors = Sector::all();
        return view('admin.sector.list', compact('sectors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sector.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg|max:2048',
            'name' => 'required'
        ]);
    
        $sector = new Sector([
            'name' => $request->get('name')
        ]);
    
        $sector->slug = str_slug($request->get('name'));
        $sector->visible = 0;
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(512, 120);
            $img->save('img/sectors/'.$filename);
            $sector->image = $filename;
        }
    
        $sector->save();
        return redirect()->route('sector_list')->with('success', 'Proyecto guardado correctamente!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = Sector::find(decrypt($id));
        return view('admin.sector.edit', compact('sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sector = Sector::find(decrypt($id));
    
        $request->validate([
            'name' => 'required'
        ]);
    
        $sector->name = request('name');
        $sector->slug = str_slug(request('name'));
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(512, 120);
            $img->save('img/sectors/'.$filename);
            $sector->image = $filename;
        }
    
        $sector->save();
        return redirect()->route('sector_edit', ['id' => $id])->with('success','Registro actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function block($id, $value){
        Sector::find(decrypt($id))->update(['visible' => $value]);
        $message = $value ? 'Sector desbloqueado correctamente.' : 'Sector bloqueado correctamente.';
        return redirect()->back()->with('success',$message);
    }
}
