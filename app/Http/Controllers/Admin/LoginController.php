<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
    * Where to redirect users after login.
    *
    * @var string
    */
    protected $redirectTo = '/admin/login';

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.login');
    }

    protected function authenticated(Request $request, $user)
    {
        if (!$user->isblocked() && !$user->isDeleted() && $user->roles()->get()->isNotEmpty()){
            $user->setSession();
        } else {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('admin/login')->withErrors(['error' => 'Comprueba el correo electrónico y la contraseña.']);
        }
    }
  
    public function logout(Request $request)
    {
      $this->guard()->logout();
      $request->session()->invalidate();
      return redirect('admin/login');
    }
}
