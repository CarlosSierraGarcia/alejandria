<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
      switch ($type){
        case 'administrator':
          $name = 'ADMIN';
        break;
        case 'entrepreneurs';
          $name = 'ENTREPRENEUR';
        break;
        case 'investors':
          $name = 'INVESTOR';
        break;
        default:
          $name = 'ADMIN';
        break;
      }
      
      $users = User::whereHas('roles', function ($role) use ($name) {
        $role->where('name', $name);
      })->where('deleted', 0)->get();
      
      return view('admin.user.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:50',
            'email'=>'required|email|max:100|unique:user',
            'dni'=>'required|max:20|unique:user',
            'phone'=>'required|max:50',
            'address'=>'required|max:100',
            'city'=>'required|max:100',
            'country'=>'required|max:100',
            'postcode'=>'required|max:25',
            'birthday'=>'required|date|max:25',
            'password'=>'required|min:8',
            'roles'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg|max:2048'
        ]);
    
        $user = new User([
            'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'dni' => $request->get('dni'),
            'birthday' => date('Y-m-d', strtotime($request->get('birthday'))),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'city' => $request->get('city'),
            'country' => $request->get('country'),
            'postcode' => $request->get('postcode'),
            'comments' => $request->get('comments'),
            'last_access' => date('Y-m-d h:i:s'),
            'num_access' => 0,
            'blocked' => 0,
            'deleted' => 0,
            'image' => 'default.jpg'
        ]);
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(64, 64);
            $img->save('img/users/'.$filename);
            $user->image = $filename;
        }
        
        $id = $user->save();
        $user->roles()->attach($request->get('roles'));
        return redirect()->route('user_edit', ['id' => encrypt($user->id)])->with('success', 'Usuario guardado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::find(decrypt($id));
      return view('admin.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = User::find(decrypt($id));
      return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::find(decrypt($id));
      
      $request->validate([
        'name' => 'required',
        'lastname' => 'required',
        'dni'=>'required|max:20',
        'phone'=>'required|max:50',
        'address'=>'required|max:100',
        'city'=>'required|max:100',
        'country'=>'required|max:100',
        'postcode'=>'required|max:25',
        'birthday'=>'required|date|max:25'
      ]);
      
      $user->name = request('name');
      $user->lastname = request('lastname');
      $user->dni = request('dni');
      $user->phone = request('phone');
      $user->address = request('address');
      $user->city = request('city');
      $user->country = request('country');
      $user->postcode = request('postcode');
      $user->comments = request('comments');
      $user->birthday = date('Y-m-d', strtotime(request('birthday')));
    
      if ($request->has('image')) {
          $image = $request->file('image');
          $filename = time().'.'.request()->image->getClientOriginalExtension();
          $img = ImageManagerStatic::make($image)->resize(64, 64);
          $img->save('img/users/'.$filename);
          $user->image = $filename;
          if (decrypt($id) == Session::get('id')){
              Session::put(['image' => $filename, 'fullname' => $user->name.' '.$user->lastname]);
          }
      }
    
      $user->save();
      return redirect()->route('user_show', ['id' => $id])->with('success','Registro actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
    }
    
    public function delete($id){
      User::find(decrypt($id))->update(['deleted' => 1]);
      return redirect()->back()->with('success','Usuario eliminado correctamente.');
    }
    
    public function block($id, $value){
      User::find(decrypt($id))->update(['blocked' => $value]);
      $message = $value ? 'Usuario bloqueado correctamente.' : 'Usuario desbloqueado correctamente.';
      return redirect()->back()->with('success',$message);
    }
}
