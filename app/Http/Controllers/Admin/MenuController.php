<?php

namespace App\Http\Controllers\Admin;

use App\Models\Phase;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function phaseMenu(){
        $phases = Phase::all();
        return $phases;
    }
    
    public function rolesMenu(){
        $roles = Role::all();
        return $roles;
    }
}
