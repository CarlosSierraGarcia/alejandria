<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($answered)
    {
        $answered = $answered == 'answered' ? 1 : 0;
        $contacts = DB::table('contact')
            ->where('answered', '=', $answered)
            ->orderBy('created_at', 'DESC')
            ->get();
        
        return view('admin.contact.list', compact('contacts'));
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find(decrypt($id));
        return view('admin.contact.show', compact('contact'));
    }
    
    public function answered($id, $value){
        Contact::find(decrypt($id))->update(['answered' => $value]);
        $message = $value ? 'Cambio de estado a respondido.' : 'Cambio de estado a pendiente.';
        return redirect()->back()->with('success',$message);
    }
}
