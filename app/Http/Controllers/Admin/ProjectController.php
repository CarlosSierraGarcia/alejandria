<?php

namespace App\Http\Controllers\Admin;

use App\Models\Investment;
use App\Models\Project;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic;
use mysql_xdevapi\Table;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $projects = Project::whereHas('phase', function ($phase) use ($type) {
            $phase->where('slug', $type);
        })->get();
        
        return view('admin.project.list', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sectors = Sector::all();
        $users = User::whereHas('roles', function ($role) {
            $role->where('name', 'ENTREPRENEUR');
        })->where('deleted', 0)->get();
        return view('admin.project.create', compact(['sectors', 'users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg|max:2048',
            'title' => 'required',
            'description' => 'required',
            'sectors' => 'required',
            'need_amount'=>'required',
            
        ]);
    
        $project = new Project([
            'title' => addslashes($request->get('title')),
            'description' => addslashes($request->get('description')),
            'need_amount' => $request->get('need_amount'),
            'image' => 'default.jpg',
            'publish_date' => date('Y-m-d h:i:s'),
            'visible' => 0,
            'visited' => 0
        ]);
        $project->user_id = $request->get('user_id');
        $project->slug = str_slug($request->get('title'));
        $project->phase_id = 1;
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(512, 120);
            $img->save('img/projects/'.$filename);
            $project->image = $filename;
        }
    
        $project->save();
        $project->sectors()->attach($request->get('sectors'));
        return redirect()->route('project_show', ['id' => encrypt($project->id)])->with('success', 'Proyecto guardado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find(decrypt($id));
        $sectors = Sector::all();
        $project_sectors = DB::table('project_sector')->select('sector_id')->where('project_id', decrypt($id))->pluck('sector_id')->toArray();
        $users = User::whereHas('roles', function ($role) {
            $role->where('name', 'ENTREPRENEUR');
        })->where('deleted', 0)->get();
        return view('admin.project.show', compact(['project', 'sectors', 'project_sectors']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find(decrypt($id));
        $sectors = Sector::all();
        $project_sectors = DB::table('project_sector')->select('sector_id')->where('project_id', decrypt($id))->pluck('sector_id')->toArray();
        $users = User::whereHas('roles', function ($role) {
            $role->where('name', 'ENTREPRENEUR');
        })->where('deleted', 0)->get();
        return view('admin.project.edit', compact(['project', 'sectors', 'users', 'project_sectors']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find(decrypt($id));
    
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'need_amount'=>'required'
        ]);
    
        $project->title = addslashes(request('title'));
        $project->description = addslashes(request('description'));
        $project->need_amount = request('need_amount');
        $project->user_id = request('user_id');
        $project->slug = str_slug(request('title'));
        $project->sectors()->sync(request('sectors'));
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(512, 120);
            $img->save('img/projects/'.$filename);
            $project->image = $filename;
        }
    
        $project->save();
        return redirect()->route('project_show', ['id' => $id])->with('success','Registro actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function accepted($id){
        $project = Project::find(decrypt($id));
        $project->phase_id = 2;
        $project->push();
        $message = 'Proyecto cambiado de fase correctamente.';
        return redirect()->back()->with('success',$message);
    }
    
    public function block($id, $value){
        Project::find(decrypt($id))->update(['visible' => $value]);
        $message = $value ? 'Projecto desbloqueado correctamente.' : 'Projecto bloqueado correctamente.';
        return redirect()->back()->with('success',$message);
    }
    
    public function projectsByUserId($id){
        return DB::table('project')->where('user_id', $id)->get();
    }
    
    public function investmentAmountByProjectId($id){
        $amount = 0;
        $investments = DB::table('investment')->where('project_id', $id)->get();
        foreach ($investments as $investment){
            $amount += $investment->investment_amount;
        }
        
        return $amount;
    }
    
    public function investmentAmountByProjectUserId($project_id, $user_id){
        $amount = 0;
        $investments = DB::table('investment')->where(['project_id' => $project_id, 'user_id' => $user_id])->get();
        foreach ($investments as $investment){
            $amount += $investment->investment_amount;
        }
        
        return $amount;
    }
    
    public function getProjectById($id){
        return Project::find($id);
    }
    
    public function numProjectsByUserId($id){
        return DB::table('project')->where('user_id', $id)->count();
    }
    
    public function investmentByUserId($id){
        return DB::table('investment')->where('user_id', $id)->get();
    }
    
    public function numInvestmentByUserId($id){
        return DB::table('investment')->where('user_id', $id)->count();
    }
    
    public function ownerByProjectId($id){
        $user = User::whereHas('project', function ($project) use ($id) {
            $project->where('id', $id);
        })->get()->toArray();
        
        return $user;
    }
    
    public function investmentOwnerByProjectId($id){
        return DB::table('investment')->where('project_id', $id)->get();
    }
    
    public function percentageProjectById($id){
        $project = Project::find($id);
        $invest_amount = $this->investmentAmountByProjectId($id);
        $need_amount = $project->need_amount;
        
        return ($invest_amount * 100)/$need_amount;
    }
}
