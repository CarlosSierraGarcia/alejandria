<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Role;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $roles = Role::all();
        $user = User::find(decrypt(Session::get('user_id')));
        return view("profile", compact(['roles', 'user']));
    }
    
    public function getRoles($user_id){
        return DB::table("role_user")->where(['user_id' => decrypt($user_id)])->get();
    }
    
    public function login(Request $request){
        $resultado = 'nok';
        $user = DB::table('user')
            ->select('password', 'id', 'name', 'lastname')
            ->where('deleted', '=', 0)
            ->where('blocked', '=', 0)
            ->where('email', '=', $request->input('email'))
            ->first();
        
        if ($user !== null) {
            if (Hash::check($request->input('password'), $user->password)) {
                Session::put(['user_id' => encrypt($user->id), 'fullname' => $user->name . ' ' . $user->lastname]);
                $resultado = 'ok';
            }
        }
    
        return response()->json(['success'=> $resultado]);
    }
    
    public function register()
    {
        $roles = Role::all();
        return view("register", compact('roles'));
    }
    
    public function create(Request $request){
        $request->validate([
            'name'=>'required|max:50',
            'email'=>'required|email|max:100|unique:user',
            'dni'=>'required|max:20|unique:user',
            'phone'=>'required|max:50',
            'address'=>'required|max:100',
            'city'=>'required|max:100',
            'country'=>'required|max:100',
            'postcode'=>'required|max:25',
            'password'=>'required|min:8',
            'roles'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg|max:2048'
        ]);
    
        $user = new User([
            'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'dni' => $request->get('dni'),
            'birthday' => '1978-02-23',
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'city' => $request->get('city'),
            'country' => $request->get('country'),
            'postcode' => $request->get('postcode'),
            'comments' => '',
            'last_access' => date('Y-m-d h:i:s'),
            'num_access' => 0,
            'blocked' => 0,
            'deleted' => 0,
            'image' => 'default.jpg'
        ]);
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(64, 64);
            $img->save('img/users/'.$filename);
            $user->image = $filename;
        }
    
        $id = $user->save();
        $user->roles()->attach($request->get('roles'));
        return redirect()->route('register', ['id' => encrypt($user->id)])->with('success', 'Usuario guardado correctamente!');
    }
    
    public function login_out(){
        Session::flush();
        return redirect()->route('inicio');
    }
    
    public function edit(Request $request, $id){
        $user = User::find(decrypt($id));
    
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'dni'=>'required|max:20',
            'phone'=>'required|max:50',
            'address'=>'required|max:100',
            'city'=>'required|max:100',
            'country'=>'required|max:100',
            'postcode'=>'required|max:25'
        ]);
    
        $user->name = request('name');
        $user->lastname = request('lastname');
        $user->dni = request('dni');
        $user->phone = request('phone');
        $user->address = request('address');
        $user->city = request('city');
        $user->country = request('country');
        $user->postcode = request('postcode');
        $user->comments = '';
    
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(64, 64);
            $img->save('img/users/'.$filename);
            $user->image = $filename;
            if (decrypt($id) == Session::get('id')){
                Session::put(['fullname' => $user->name.' '.$user->lastname]);
            }
        }
    
        $user->save();
        return redirect()->route('profile')->with('success','Usuario actualizado correctamente.');
    }
    
    public function my_projects(){
        $projects = DB::table('project')->where(['user_id' => decrypt(Session::get('user_id')), 'visible' => 1])->get();
        return view('my-projects', compact('projects'));
    }
    
    public function my_investments(){
        $investments = DB::table('investment')
            ->join('project', 'project.id', '=', 'investment.project_id')
            ->where(['investment.user_id' => decrypt(Session::get('user_id'))])->get();
        
        return view('my-investments', compact('investments'));
    }
    
    public function investmentOwnerByProjectId($id){
        return DB::table('investment')->where('project_id', $id)->get();
    }
    
    public function getInvestmentByProjectId($id){
        return DB::table('investment')->where('project_id', $id)->sum('investment_amount');
    }
    
    public function my_projects_create(){
        $roles = Role::all();
        $user = User::find(decrypt(Session::get('user_id')));
        $sectors = Sector::all();
        return view("my-projects-create", compact(['roles', 'user', 'sectors']));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function my_projects_store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg|max:2048',
            'title' => 'required',
            'description' => 'required',
            'sectors' => 'required',
            'need_amount'=>'required',
        ]);
        
        $project = new Project([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'need_amount' => $request->get('need_amount'),
            'image' => 'default.jpg',
            'publish_date' => date('Y-m-d h:i:s'),
            'visible' => 0,
            'visited' => 0
        ]);
        $project->user_id = decrypt(Session::get('user_id'));
        $project->slug = str_slug($request->get('title'));
        $project->phase_id = 1;
        
        if ($request->has('image')) {
            $image = $request->file('image');
            $filename = time().'.'.request()->image->getClientOriginalExtension();
            $img = ImageManagerStatic::make($image)->resize(512, 120);
            $img->save('img/projects/'.$filename);
            $project->image = $filename;
        }
        
        $project->save();
        $project->sectors()->attach($request->get('sectors'));
        return redirect()->route('my_projects')->with('success', 'Proyecto guardado correctamente!. Tu proyecto será estudiado por nuestro equipo lo antes posible.');
    }
}
