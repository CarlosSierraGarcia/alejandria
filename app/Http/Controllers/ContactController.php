<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("contact");
    }
    
    public function send(Request $request){
        $request->validate([
            'name' => 'required',
            'email'=>'required|email',
            'subject' => 'required',
            'message'=>'required'
        ]);
    
        $contact = new Contact([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'message' => $request->get('message'),
            'answered' => false,
            'created_at' => date('Y-m-d h:i:s')
        ]);
    
        $contact->save();
        return redirect()->route('contact')->with('success', 'Mensaje enviado correctamente!');
    }
}
