<?php

namespace App\Http\Controllers;

use App\Models\Investment;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1)
    {
        $limit = 50;
        $start = ($page > 1) ? $page * $limit : 0;
        if (!empty($page) && !is_numeric($page)){
            $projects = DB::table('project')
                ->select('project.*', 'project.id')
                ->join('project_sector', 'project.id', '=', 'project_sector.project_id')
                ->join('sector', 'sector.id', '=', 'project_sector.sector_id')
                ->where('project.visible', '=', true)
                ->where('sector.visible', '=', true)
                ->where('sector.slug', '=', $page)
                ->skip($start)
                ->take($limit)
                ->get();
    
            $total = DB::table('project')
                ->join('project_sector', 'project.id', '=', 'project_sector.project_id')
                ->join('sector', 'sector.id', '=', 'project_sector.sector_id')
                ->where('project.visible', '=', true)
                ->where('sector.visible', '=', true)
                ->where('sector.slug', '=', $page)
                ->count();
        } else {
            $projects = DB::table('project')
                ->select('project.*', 'project.id')
                ->join('project_sector', 'project.id', '=', 'project_sector.project_id')
                ->join('sector', 'sector.id', '=', 'project_sector.sector_id')
                ->where('project.visible', '=', true)
                ->where('sector.visible', '=', true)
                ->skip($start)
                ->take($limit)
                ->get();
    
            $total = DB::table('project')
                ->where('project.visible', '=', true)
                ->count();
        }
        
        $sectors = $this->getSectors();
        
        return view("projects", compact(['projects', 'sectors', 'page', 'total']));
    }
    
    public function getSectors(){
        return DB::table('sector')
            ->where('visible','=', true)
            ->get();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function interna($slug)
    {
        $project = DB::table('project')
            ->select('project.*')
            ->join('project_sector', 'project.id', '=', 'project_sector.project_id')
            ->join('sector', 'sector.id', '=', 'project_sector.sector_id')
            ->where('project.visible', '=', true)
            ->Where('project.slug', '=', $slug)
            ->where('sector.visible', '=', true)
            ->first();
        
        
        return view("project_interna", compact('project'));
    }
    
    public function search(Request $request) {
        $projects = DB::table('project')
            ->select('id', 'title')
            ->where('title', 'like', '%'.$request->get('title').'%')
            ->orderby('title', 'ASC')
            ->get();
        
        echo json_encode($projects);
    }
    
    public function investment(Request $request, $id){
        $request->validate([
            'owner' => 'required',
            'investment_amount' => 'required'
        ]);
        
        $project = Project::find(decrypt($id));
        if ($project && $request->session()->exists('user_id')){
            $investment = new Investment([
                'owner' => $request->get('owner'),
                'investment_amount' => $request->get('investment_amount'),
                
            ]);
            $investment->user_id = decrypt(Session::get('user_id'));
            $investment->project_id = $project->id;
    
            $investment->save();
            return redirect()->route('project_interna', ['slug' => $project->slug])->with('success','La inversión se ha completado correctamente.');
        } else {
            return redirect()->route('project_interna', ['slug' => $project->slug])->with('error','Se ha producido un error.');
        }
    }
}
