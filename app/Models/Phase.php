<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $table = 'phase';
    protected $fillable = ['name', 'slug'];
}
