<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = ['title', 'body', 'author', 'image', 'visible', 'slug', 'created_at', 'update_at'];
    
    /**
     * WrittenBy belongs to User.
     */
    public function writtenBy()
    {
        return $this->hasOne(User::class);
    }
    
    /**
     * Category belongs to User.
     */
    public function category()
    {
        return $this->belongsTo(NewsCategory::class);
    }
}
