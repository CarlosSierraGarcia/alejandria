<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    protected $fillable = ['name', 'slug', 'text'];

    /**
    * The users that belong to the role.
    */
    public function users()
    {
      return $this->belongsToMany(User::class);
    }
}
