<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $table = 'investment';
    protected $fillable = ['owner', 'investment_amount'];
    
    /**
     * Investments belongs to User.
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
    
    /**
     * Investments belongs to Project.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
