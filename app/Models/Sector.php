<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'sector';
    protected $fillable = ['name', 'slug', 'image', 'visible'];
    public $timestamps = false;

    /**
     * The users that belong to the role.
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
}
