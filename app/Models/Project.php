<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $fillable = ['name', 'image', 'title', 'description', 'slug', 'publish_date', 'visible', 'need_amount', 'visited', 'created_at', 'updated_at'];

    /**
     * The phases that belong to the project.
     */
    public function phase()
    {
        return $this->belongsTo(Phase::class);
    }

    /**
     * The sectors that belong to the project.
     */
    public function sectors()
    {
        return $this->belongsToMany(Sector::class);
    }

    /**
     * The owner that belong to the project.
     */
    public function owner()
    {
        return $this->hasOne(User::class);
    }
}
