<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;


class User extends Authenticatable
{
  protected $table = 'user';
  protected $fillable = ['name', 'lastname', 'birthday', 'dni', 'phone', 'address', 'city', 'country', 'postcode', 'email', 'image', 'registration_date', 'last_access', 'num_access', 'comments', 'blocked', 'deleted', 'password'];
  protected $remember_token = false;

  /**
   * The roles that belong to the user.
   */
  public function roles()
  {
    return $this->belongsToMany(Role::class);
  }
  
  public function project(){
    return $this->hasOne(Project::class);
  }

  public function setSession(){
      Session::put([
          'id' => $this->id,
          'fullname' => $this->name.' '.$this->lastname,
          'image' => $this->image,
          'role' => $this->roles()->get()->toArray(),
          'lockscreen' => false
      ]);
  }

  public function isBlocked(){
      return $this->blocked;
  }

  public function isDeleted(){
      return $this->deleted;
  }
  
  public function hasRole($role){
      $resul = false;
      foreach ($this->roles->toArray() as $user_roles){
         if ($user_roles['name'] == $role){
             $resul = true;
         }
      }
      
      return $resul;
  }
}
