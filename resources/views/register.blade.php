@extends("theme.front.layout")
@section('style')

@endsection

@section('content')
    @include('theme.front.search')
    <section class="ftco-section contact-section">
        <div class="container">
            <div class="row d-flex mb-5 contact-info justify-content-center">
                <div class="col-md-8">
                    <div class="row mb-5">
                        <div class="col-md-4 text-center py-4">
                            <div class="icon">
                                <span class="icon-map-o"></span>
                            </div>
                            <p><span>Dirección:</span> Paseo de la castellana 33, Madrid, España</p>
                        </div>
                        <div class="col-md-4 text-center border-height py-4">
                            <div class="icon">
                                <span class="icon-mobile-phone"></span>
                            </div>
                            <p><span>Teléfono:</span> <a href="tel://689075043">689 07 50 43</a></p>
                        </div>
                        <div class="col-md-4 text-center py-4">
                            <div class="icon">
                                <span class="icon-envelope-o"></span>
                            </div>
                            <p><span>E-mail:</span> <a href="mailto:carlos.sierra.garcia@gmail.com">carlos.sierra.garcia@gmail.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row block-9 justify-content-center mb-5">
                <div class="col-md-8 mb-md-5">
                    <h2 class="text-center">Registrate en 5 minutos <br>y empieza a crear tu sueño</h2>
                    <form action="{{ route('create') }}" class="bg-light p-5 contact-form" method="post" autocomplete="off" enctype="multipart/form-data">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                                {{ $message }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i>Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="form-group">
                            <label>Imagen</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input type="text" class="form-control" name="dni" value="{{ old('dni') }}">
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                        </div>
                        <div class="form-group">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                        </div>
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                        </div>
                        <div class="form-group">
                            <label>País</label>
                            <input type="text" class="form-control" name="country" value="{{ old('country') }}">
                        </div>
                        <div class="form-group">
                            <label>Código postal</label>
                            <input type="text" class="form-control" name="postcode" value="{{ old('postcode') }}">
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>Roles</label>
                            @foreach($roles as $i => $role)
                                <div>
                                    @if ($role->name == 'ENTREPRENEUR')
                                        <input type="checkbox" class="flat-red" name="roles[]" value="{{ $role->id }}" {{is_array(old('roles')) && in_array($role->id, old('roles')) ? 'checked' : ''}}>
                                        <label>Emprendedor</label>
                                    @elseif ($role->name == 'INVESTOR')
                                        <input type="checkbox" class="flat-red" name="roles[]" value="{{ $role->id }}" {{is_array(old('roles')) && in_array($role->id, old('roles')) ? 'checked' : ''}}>
                                        <label>Inversor</label>
                                    @endif
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Enviar" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
@endsection
