@extends("theme.front.layout")
@section('style')

@endsection

@section('content')
    <section class="ftco-section contact-section">
        <div class="container">
            @include('theme.front.user_menu')
            <div class="row block-9 justify-content-center mb-5">
                <div class="col-md-8 mb-md-5">
                    <h2 class="text-center">Nuevo Proyecto</h2>
                    <form role="form" action="{{ route('my_projects_store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                                {{ $message }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i>Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label>Imagen</label>
                                <input type="file" class="form-control" name="image">
                            </div>
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" class="form-control" name="title" {{ old('title') }}>
                            </div>
                            <div class="form-group">
                                <label>Descripción</label><br/>
                                <textarea id="editor1" name="description" rows="10" cols="80">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Cantidad necesaria</label>
                                <input type="text" class="form-control" name="need_amount" {{ old('need_amount') }}>
                            </div>
                            <div class="form-group">
                                <label>Sector</label>
                                @foreach($sectors as $sector)
                                    <div>
                                        <input type="checkbox" class="flat-red" name="sectors[]" value="{{ $sector->id }}" {{is_array(old('sectors')) && in_array($sector->id, old('sectors')) ? 'checked' : ''}}>
                                        <label>{{ $sector->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
<!-- CK Editor -->
<script src="{{asset("assets/$theme/bower_components/ckeditor/ckeditor.js")}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset("assets/$theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
<script>
    $(function () {
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
@endsection
