<!DOCTYPE html>
<html lang="en">
<head>
    <title>Alejandría | Tu idea, tu negocio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset("assets/front/css/open-iconic-bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/front/css/animate.css")}}">

    <link rel="stylesheet" href="{{asset("assets/front/css/owl.carousel.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/front/css/owl.theme.default.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/front/css/magnific-popup.css")}}">

    <link rel="stylesheet" href="{{asset("assets/front/css/aos.css")}}">

    <link rel="stylesheet" href="{{asset("assets/front/css/ionicons.min.css")}}">

    <link rel="stylesheet" href="{{asset("assets/front/css/bootstrap-datepicker.css")}}">
    <link rel="stylesheet" href="{{asset("assets/front/css/jquery.timepicker.css")}}">


    <link rel="stylesheet" href="{{asset("assets/front/css/flaticon.css")}}">
    <link rel="stylesheet" href="{{asset("assets/front/css/icomoon.css")}}">
    <link rel="stylesheet" href="{{asset("assets/front/css/style.css")}}">
    <link rel="stylesheet" href="{{asset("css/front/base.css")}}">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('styles')
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.html">Alejandría</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ request()->segment(1) == '' ? 'active' : '' }}"><a href="{{ route('inicio') }}" class="nav-link">Home</a></li>
                <li class="nav-item {{ request()->segment(1) == 'about-us' ? 'active' : '' }}"><a href="{{ route('about_us') }}" class="nav-link">Sobre nosotros</a></li>
                <li class="nav-item {{ request()->segment(1) == 'projects' ? 'active' : '' }}"><a href="{{ route('projects') }}" class="nav-link">Proyectos</a></li>
                <li class="nav-item {{ request()->segment(1) == 'blog' ? 'active' : '' }}"><a href="{{ route('blog') }}" class="nav-link">Blog</a></li>
                <li class="nav-item {{ request()->segment(1) == 'contact' ? 'active' : '' }}"><a href="{{ route('contact') }}" class="nav-link">Contacto</a></li>
                @if (session()->has('user_id'))
                    <li class="nav-item {{ request()->segment(1) == 'profile' ? 'active' : '' }}"><a href="{{ route('profile') }}" class="nav-link">{{ Session::get('fullname') }}</a></li>
                @else
                    <li class="nav-item"><a href="javascript: void(0)" class="nav-link" data-toggle="modal" data-target="#modal-login">Inicio</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@yield('content')

@include("theme/front/footer")
<!-- Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-login" aria-hidden="true">
    <form id="login" role="form" action="" onsubmit="return validate_login()" method="post" autocomplete="off">
        @csrf
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <div class="alert-text">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            </div>
        @endif
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Iniciar sesión</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" id="email_login" required class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" id="password_login" required class="form-control" name="password" value="{{ old('password') }}">
                        </div>
                        <div class="form-group">
                            <p id="message" style="color: red"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('register') }}" class="btn btn-primary">Registrarse</a>
                    <button type="submit" class="btn btn-primary">Acceder</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="{{asset("assets/front/js/jquery.min.js")}}"></script>
<script src="{{asset("assets/front/js/jquery-migrate-3.0.1.min.js")}}"></script>
<script src="{{asset("assets/front/js/popper.min.js")}}"></script>
<script src="{{asset("assets/front/js/bootstrap.min.js")}}"></script>
<script src="{{asset("assets/front/js/jquery.easing.1.3.js")}}"></script>
<script src="{{asset("assets/front/js/jquery.waypoints.min.js")}}"></script>
<script src="{{asset("assets/front/js/jquery.stellar.min.js")}}"></script>
<script src="{{asset("assets/front/js/owl.carousel.min.js")}}"></script>
<script src="{{asset("assets/front/js/jquery.magnific-popup.min.js")}}"></script>
<script src="{{asset("assets/front/js/aos.js")}}"></script>
<script src="{{asset("assets/front/js/jquery.animateNumber.min.js")}}"></script>
<script src="{{asset("assets/front/js/bootstrap-datepicker.js")}}"></script>
<script src="{{asset("assets/front/js/jquery.timepicker.min.js")}}"></script>
<script src="{{asset("assets/front/js/scrollax.min.js")}}"></script>
<script src="{{asset("assets/front/js/main.js")}}"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/eggplant/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
@yield('javascript')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function validate_login(){
        $('#modal-login').removeClass('shake');
        $('#message').html('');
        $.ajax({
            type: "POST",
            url: "{{ route('user_login') }}",
            dataType: "json",
            data: {
                email: $('#email_login').val(),
                password: $('#password_login').val()
            },
            success: function(data) {
                if (data.success == 'ok'){
                    location.reload();
                } else {
                    $('#modal-login').addClass('shake');
                    $('#message').html('Contraseña incorrecta.');
                }
            },
        });

        return false;
    }

    $("#search-project").autocomplete({
        minLength : 2,
        source : function(request, response) {
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                url : "{{ route('search_project') }}",
                dataType : "json",
                data : {
                    title: request.term
                },
                success : function(data) {
                    $('.ui-helper-hidden-accessible').hide();
                    response($.map(data, function(item) {
                        return {
                            label : item.title,
                            value: item.id
                        }
                    }));
                }
            });
        },
        select : function(event, ui) {
            $('input[name="project_id"]').val(ui.item.value);
            $('#search-project').val(ui.item.label);

            return false;
        }
    });
</script>
</body>
</html>
