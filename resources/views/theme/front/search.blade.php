<div class="hero-wrap ftco-degree-bg" style="background-image: url('{{asset("assets/front/images/test.jpg")}}');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text justify-content-center align-items-center">
            <div class="col-lg-8 col-md-6 ftco-animate d-flex align-items-end">
                <div class="text text-center">
                    <h1 class="mb-4">Convierte Tu Idea <br>En Tu Negocio</h1>
                    <p style="font-size: 18px;">En Alejandría convertimos tus ideas en negocios y buscamos inversion a traves de nuestra red de inversores.</p>
                    <form action="#" class="search-location mt-md-5">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 align-items-end">
                                <div class="form-group">
                                    <div class="form-field">
                                        <input type="hidden" name="project_id" value="0">
                                        <input type="text" class="form-control" id="search-project" placeholder="Search location">
                                        <button><span class="ion-ios-search"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mouse">
        <a href="#" class="mouse-icon">
            <div class="mouse-wheel"><span class="ion-ios-arrow-round-down"></span></div>
        </a>
    </div>
</div>
