<div class="row d-flex mb-5 contact-info justify-content-center">
    <div class="col-md-12">
        <div class="row mb-5 user-menu">
            @inject("userController", "App\Http\Controllers\UserController")
            <div class="col-md-3 text-center py-4">
                <a href="{{ route('profile') }}" {{ request()->segment(1) == 'profile' && request()->segment(2) == '' ? 'class=active' : '' }}>
                    <div class="icon">
                        <span class="icon-user"></span>
                    </div>
                    <p><span>Mi Perfil</span></p>
                </a>
            </div>
            @if (sizeof($userController->getRoles(Session::get('user_id'))) > 0)
                @foreach($userController->getRoles(Session::get('user_id')) as $role)
                    @if ($role->role_id == 2)
                        <div class="col-md-3 text-center border-height py-4">
                            <a href="{{ route('my_investments') }}" {{ request()->segment(2) == 'my-investments' ? 'class=active' : '' }}>
                                <div class="icon">
                                    <span class="icon-money"></span>
                                </div>
                                <p><span>Mis inversiones</span></p>
                            </a>
                        </div>
                    @elseif ($role->role_id == 3)
                        <div class="col-md-3 text-center border-height py-4">
                            <a href="{{ route('my_projects') }}" {{ request()->segment(2) == 'my-projects' ? 'class=active' : '' }}>
                                <div class="icon">
                                    <span class="icon-rocket"></span>
                                </div>
                                <p><span>Mis proyectos</span></p>
                            </a>
                        </div>
                    @elseif ($role->role_id != 1)
                        <div class="col-md-3 text-center border-height py-4">
                        </div>
                    @endif
                @endforeach
            @elseif (sizeof($userController->getRoles(Session::get('user_id'))) == 0)
                <div class="col-md-3 text-center border-height py-4">
                </div>
                <div class="col-md-3 text-center border-height py-4">
                </div>
            @endif
            <div class="col-md-3 text-center py-4">
                <a href="{{ route('login_out') }}">
                    <div class="icon">
                        <span class="icon-exit_to_app"></span>
                    </div>
                    <p><span>Desconectar</span></p>
                </a>
            </div>
        </div>
    </div>
</div>
