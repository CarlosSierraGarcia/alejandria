<footer class="ftco-footer ftco-section">
    <div class="container my-footer">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Alejandria</h2>
                    <p>En Alejandría convertimos tus ideas en negocios y buscamos inversion a traves de nuestra red de inversores.</p>
                    <p>Tu idea, tu negocio.</p>
                    <ul class="ftco-footer-social list-unstyled mt-5">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">Blog</h2>
                    <ul class="list-unstyled">
                        @inject("blogController", "App\Http\Controllers\BlogController")
                        @foreach($blogController->getCategories() as $category)
                            <li><a href="{{ route('blog_category', ['slug' => $category->slug]) }}"><span class="icon-long-arrow-right mr-2"></span>{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">Proyectos</h2>
                    <ul class="list-unstyled">
                        @inject("projectController", "App\Http\Controllers\ProjectController")
                        @foreach($projectController->getSectors() as $sector)
                        <li><a href="{{ route('projects', ['page' => $sector->slug]) }}"><span class="icon-long-arrow-right mr-2"></span>{{ $sector->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">Sobre Nosotros</h2>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('contact') }}"><span class="icon-long-arrow-right mr-2"></span>Equipo</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">¿Tienes alguna duda?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">Paseo de la castellana 33, Madrid, España</span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">689 07 50 43</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope pr-4"></span><span class="text">carlos.sierra.garcia@gmail.com</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
