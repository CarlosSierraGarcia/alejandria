<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LJ</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Alejandría</b> BackOffice</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown tasks-menu">
                    <a href="" id="btn-lockscreen">
                        <i class="fa fa-lock"></i>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ asset("img/users/".Session::get('image')) }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Session::get('fullname') }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ asset("img/users/".Session::get('image')) }}" class="img-circle" alt="User Image">
                            <p>
                                {{ Session::get('fullname') }}
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('user_edit', ['id' => encrypt(Session::get('id'))]) }}" class="btn btn-default btn-flat">Editar</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout' )}}" class="btn btn-default btn-flat">Salir</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
