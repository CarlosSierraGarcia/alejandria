<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset("img/users/".Session::get('image'))}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Session::get('fullname') }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Administrador</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            @inject("menu", "App\Http\Controllers\Admin\MenuController")
            <li class="header">Menu Principal</li>
            <li class="{{ (request()->is('admin')) ? 'active' : '' }}"><a href="{{route('opening')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="{{ (request()->is('admin/users/*')) ? 'active' : '' }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Usuarios</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>

                <ul class="treeview-menu">
                    @foreach($menu->rolesMenu() as $role)
                        <li {{ (request()->is('admin/users/'.$role->slug)) ? 'class=active' : '' }}><a href="{{route('users_list', ['type' => $role->slug])}}"><i class="fa fa-circle-o"></i> {{ $role->text }}</a></li>
                    @endforeach
                </ul>
            </li>
            <li class="{{ (request()->is('admin/projects/*')) ? 'active' : '' }} treeview">
                <a href="#">
                    <i class="fa fa-rocket"></i>
                    <span>Proyectos</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    @foreach($menu->phaseMenu() as $phase)
                        <li {{ (request()->is('admin/projects/'.$phase->slug)) ? 'class=active' : '' }}><a href="{{route('project_list', ['type' => $phase->slug])}}"><i class="fa fa-circle-o"></i> {{$phase->name}}</a></li>
                    @endforeach
                </ul>
            </li>
            <li class="{{ (request()->is('admin/sectors/*')) ? 'active' : '' }}"><a href="{{route('sector_list')}}"><i class="fa fa-sitemap"></i> <span>Sectores</span></a></li>
            <li class="{{ (request()->is('admin/news/*') || request()->is('admin/categories/*')) ? 'active' : '' }} treeview">
                <a href="#">
                    <i class="fa fa-th"></i>
                    <span>Blog</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>

                <ul class="treeview-menu">
                    <li {{ (request()->is('admin/news/list')) ? 'class=active' : '' }}><a href="{{route('news_list')}}"><i class="fa fa-newspaper-o"></i> Noticias</a></li>
                    <li {{ (request()->is('admin/categories/list')) ? 'class=active' : '' }}><a href="{{route('category_list')}}"><i class="fa fa-tasks"></i> Categorias</a></li>
                </ul>
            </li>
            <li class="{{ (request()->is('admin/investment/*')) ? 'active' : '' }}"><a href="{{route('investment_list')}}"><i class="fa fa-money"></i> <span>Inversión</span></a></li>
            <li class="{{ (request()->is('admin/contact/*')) ? 'active' : '' }} treeview">
                <a href="#">
                    <i class="fa fa-envelope-o"></i>
                    <span>Contacto</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>

                <ul class="treeview-menu">
                    <li {{ (request()->is('admin/contact/list/pending')) ? 'class=active' : '' }}><a href="{{route('contact_list', ['answered' => 'pending'])}}"><i class="fa fa-circle-o"></i> Pendiente</a></li>
                    <li {{ (request()->is('admin/contact/list/answered')) ? 'class=active' : '' }}><a href="{{route('contact_list', ['answered' => 'answered'])}}"><i class="fa fa-circle-o"></i> Respondido</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
