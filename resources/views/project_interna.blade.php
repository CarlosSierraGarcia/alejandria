@extends("theme.front.layout")

@section('content')
    @include('theme.front.search')
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                    {{ $message }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <div class="alert-text">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 order-md-last ftco-animate">
                    <h1 class="mb-3 center"><strong>{{ $project->title }}</strong></h1>
                    {!! $project->description !!}
                </div> <!-- .col-md-8 -->
            </div>
            <div class="project-data">
                @inject("userController", "App\Http\Controllers\UserController")
                <p class="right"><strong>Total Necesario:</strong> {{ number_format($project->need_amount, 2) }}&euro;</p>
                <p class="right"><strong>Total Invertido:</strong> {{ number_format($userController->getInvestmentByProjectId($project->id), 2) }}&euro;</p>
                <div class="center">
                    <a href="javascript: void(0)" class="btn btn-primary center" data-toggle="modal" data-target="{{ session()->has('user_id') ? '#modal-project' : '#modal-login' }}">INVERTIR</a>
                </div>
            </div>
        </div>
    </section> <!-- .section -->

    <!-- Modal -->
    <div class="modal fade" id="modal-project" tabindex="-1" role="dialog" aria-labelledby="modal-project" aria-hidden="true">
        <form id="project" role="form" action="{{ route('project_investment', ['id' => encrypt($project->id)]) }}" method="post" autocomplete="off">
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <div class="alert-text">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ $project->title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nombre y Apellidos</label>
                                <input type="text" id="owner" required class="form-control" name="owner">
                            </div>
                            <div class="form-group">
                                <label>Cantidad que desea invertir</label>
                                <input type="number" id="investment_amount" required class="form-control" name="investment_amount">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Invertir</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('javascript')
@endsection
