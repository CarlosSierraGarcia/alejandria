@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Sector
        <small>
            {{ $sector->title }}
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" action="{{ route('sector_update', encrypt($sector->id)) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="box-body">
                        <div class="form-group">
                            <label>Imagen</label>
                            <img class="img-responsive pad" src="{{asset("img/sectors/".$sector->image)}}" alt="Photo">
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Título</label>
                            <input type="text" class="form-control" name="name" value="{{$sector->name}}">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
@endsection
