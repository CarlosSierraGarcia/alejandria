@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Sectores
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                        <a href="{{ route('sector_create') }}"><i class="fa fa-plus"><span>Nuevo Sector</span></i></a>
                    </div>
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID#</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sectors as $sector)
                            <tr {{ $sector->visible ? '' : 'class=blocked' }}>
                                <td>{{ $sector->id }}</td>
                                <td>{{ $sector->name }}</td>
                                <td width="12%">
                                    <a class="btn btn-actions" href="{{ route('sector_edit', ['id' => encrypt($sector->id)]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    @if ($sector->visible)
                                        <a class="btn btn-actions" href="{{ route('sector_block', ['id' => encrypt($sector->id), 'value' => 0]) }}">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-actions" href="{{ route('sector_block', ['id' => encrypt($sector->id), 'value' => 1]) }}">
                                            <i class="fa fa-power-off"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{asset("assets/$theme/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("assets/$theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script>
        $(function () {
            $('#table').DataTable({
                "bLengthChange" : false,
                "bInfo":false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        })
    </script>
@endsection

