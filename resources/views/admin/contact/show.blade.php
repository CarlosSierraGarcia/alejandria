@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Contacto
        <small>
            {{ $contact->name }}
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Usuario</label>
                        <input type="text" class="form-control" disabled value="{{$contact->name}}">
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" class="form-control" disabled value="{{$contact->email}}">
                    </div>
                    <div class="form-group">
                        <label>Asunto</label>
                        <input type="text" class="form-control" disabled value="{{$contact->subject}}">
                    </div>
                    <div class="form-group">
                        <label>Mensaje</label><br/>
                        <textarea disabled rows="10" cols="80">{{ $contact->message }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Acciones</label><br/>
                        @if ($contact->answered)
                            <a class="btn btn-actions" href="{{ route('contact_answered', ['id' => encrypt($contact->id), 'value' => 0]) }}">
                                <i class="fa fa-check"></i>
                            </a>
                        @else
                            <a class="btn btn-actions" href="{{ route('contact_answered', ['id' => encrypt($contact->id), 'value' => 1]) }}">
                                <i class="fa fa-power-off"></i>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
@endsection
