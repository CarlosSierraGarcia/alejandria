@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Contactos
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID#</th>
                            <th>Usuario</th>
                            <th>E-mail</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{ $contact->id }}</td>
                                <td>{{ $contact->name }}</td>
                                <td>{{ $contact->email }}</td>
                                <td>{{ date('d/m/Y H:i:s', strtotime($contact->created_at)) }}</td>
                                <td width="12%">
                                    <a class="btn btn-actions" href="{{ route('contact_show', ['id' => encrypt($contact->id)]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    @if ($contact->answered)
                                        <a class="btn btn-actions" href="{{ route('contact_answered', ['id' => encrypt($contact->id), 'value' => 0]) }}">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-actions" href="{{ route('contact_answered', ['id' => encrypt($contact->id), 'value' => 1]) }}">
                                            <i class="fa fa-power-off"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{asset("assets/$theme/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("assets/$theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script>
        $(function () {
            $('#table').DataTable({
                "bLengthChange" : false,
                "bInfo":false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        })
    </script>
@endsection

