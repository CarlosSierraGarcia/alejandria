@extends("theme.$theme.layout")

@section('styles')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset("assets/$theme/plugins/iCheck/all.css")}}">
@endsection

@section('content-header')
    <h1>
        Proyectos
        <small>
            {{ $project->title }}
        </small>
    </h1>
@endsection

@section('content')
    @inject("projectController", "App\Http\Controllers\Admin\ProjectController")
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Imagen</label>
                        <img class="img-responsive pad" src="{{asset("img/projects/".$project->image)}}" alt="Photo">
                        <div class="progress progress-bar-site progress-striped active">
                            <div class="progress-bar percentage-bar
                            @if ($projectController->percentageProjectById($project->id) < 30)
                                progress-bar-danger
                            @elseif ($projectController->percentageProjectById($project->id) >= 30 && ($projectController->percentageProjectById($project->id) < 60))
                                progress-bar-yellow
                            @elseif ($projectController->percentageProjectById($project->id) >= 60)
                                progress-bar-success
                            @endif
                                "><strong>{{ number_format($projectController->percentageProjectById($project->id), 2) }}%</strong></div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-2">
                        @php
                            $user = $projectController->ownerByProjectId($project->id);
                        @endphp
                        <label>Emprendedor</label>
                        <div style="display: flex;">
                            <input type="text" class="form-control" disabled value="{{$user[0]['name'].' '.$user[0]['lastname']}}">
                            <a class="btn btn-actions" target="_blank" href="{{ route('user_show', ['id' => encrypt($user[0]['id'])]) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Título</label>
                        <input type="text" class="form-control" disabled value="{{$project->title}}">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Descripción</label>
                        <textarea id="editor1" disabled rows="10" cols="80">{{ $project->description }}</textarea>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Cantidad necesaria</label>
                        <input type="text" class="form-control" disabled value="{{ number_format($project->need_amount, 2) }} &euro;">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Cantidad invertida</label>
                        <input type="text" class="form-control" disabled value="{{ number_format($projectController->investmentAmountByProjectId($project->id), 2) }} &euro;">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Fecha de creación</label>
                        <input type="text" class="form-control" disabled value="{{ date('d/m/Y H:i:s', strtotime($project->created_at)) }}">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Sector</label>
                        @foreach($sectors as $sector)
                            <div>
                                <input type="checkbox" class="flat-red" value="{{ $sector->id }}" {{in_array($sector->id, $project_sectors) ? 'checked' : ''}}>
                                <label>{{ $sector->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('project_edit', ['id' => encrypt($project->id)]) }}" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Inversión</h3>
                </div>
                <!-- form start -->
                <div class="box-body">
                    @if ($projectController->investmentAmountByProjectId($project->id) > 0)
                        <table id="table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID#</th>
                                    <th>Nombre</th>
                                    <th>Cantidad invertida</th>
                                    <th>Fecha de publicación</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($projectController->investmentOwnerByProjectId($project->id) as $investment)
                                <tr>
                                    <td>{{ $investment->id }}</td>
                                    <td>{{ $investment->owner }}</td>
                                    <td>{{ number_format($investment->investment_amount, 2) }} &euro;</td>
                                    <td>{{ date('d/m/Y H:i:s', strtotime($project->created_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p><strong>Sin inversiones asociadas.</strong></p>
                    @endif
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
<!-- iCheck 1.0.1 -->
<script src="{{asset("assets/$theme/plugins/iCheck/icheck.min.js")}}"></script>
<!-- CK Editor -->
<script src="{{asset("assets/$theme/bower_components/ckeditor/ckeditor.js")}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset("assets/$theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
<script>
    $(function () {
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })
        $('.progress-bar').css('width', {{ number_format($projectController->percentageProjectById($project->id), 2) }}+'%');
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
@endsection
