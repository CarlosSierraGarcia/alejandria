@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Proyectos
    </h1>
@endsection

@section('content')
    @inject("investment", "App\Http\Controllers\Admin\ProjectController")
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                        <a href="{{ route('project_create') }}"><i class="fa fa-plus"><span>Nuevo Proyecto</span></i></a>
                    </div>
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID#</th>
                            <th>Nombre</th>
                            <th>Fecha de publicación</th>
                            <th>Cantidad necesaria</th>
                            <th>Cantidad invertida</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr {{ $project->visible ? '' : 'class=blocked' }}>
                                <td>{{ $project->id }}</td>
                                <td>{{ $project->title }}</td>
                                <td>{{ date('d/m/Y H:i:s', strtotime($project->created_at)) }}</td>
                                <td>{{ number_format($project->need_amount, 2) }} &euro;</td>
                                <td>{{ number_format($investment->investmentAmountByProjectId($project->id), 2) }} &euro;</td>
                                <td width="12%">
                                    <a class="btn btn-actions"  href="{{ route('project_show', ['id' => encrypt($project->id)]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a class="btn btn-actions" href="{{ route('project_edit', ['id' => encrypt($project->id)]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    @if ($project->phase_id == 1)
                                        <a class="btn btn-actions" href="{{ route('project_accepted', ['id' => encrypt($project->id)]) }}">
                                            <i class="fa fa-thumbs-o-up"></i>
                                        </a>
                                    @elseif ($project->visible)
                                        <a class="btn btn-actions" href="{{ route('project_block', ['id' => encrypt($project->id), 'value' => 0]) }}">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-actions" href="{{ route('project_block', ['id' => encrypt($project->id), 'value' => 1]) }}">
                                            <i class="fa fa-power-off"></i>
                                        </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{asset("assets/$theme/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("assets/$theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script>
        $(function () {
            $('#table').DataTable({
                "bLengthChange" : false,
                "bInfo":false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        })
    </script>
@endsection

