@extends("theme.$theme.layout")

@section('styles')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset("assets/$theme/plugins/iCheck/all.css")}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset("assets/$theme/bower_components/select2/dist/css/select2.min.css")}}">
@endsection

@section('content-header')
    <h1>
        Proyectos
        <small>
            {{ $project->title }}
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" action="{{ route('project_update', encrypt($project->id)) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="box-body">
                        <div class="form-group">
                            <label>Imagen</label>
                            <img class="img-responsive pad" src="{{asset("img/projects/".$project->image)}}" alt="Photo">
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Emprendedor</label>
                            <select class="form-control select2" name="user_id" style="width: 100%;">
                                @foreach($users as $user)
                                <option {{ ($user->id == $project->user_id) ? 'selected' : '' }}  value="{{ $user->id }}">{{ $user->name.' '.$user->lastname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Título</label>
                            <input type="text" class="form-control" name="title" value="{{$project->title}}">
                        </div>
                        <div class="form-group">
                            <label>Descripción</label>
                            <textarea id="editor1" name="description" rows="10" cols="80">{{ $project->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Cantidad necesaria</label>
                            <input type="text" class="form-control" name="need_amount" value="{{ $project->need_amount }}">
                        </div>
                        <div class="form-group">
                            <label>Sector</label>
                            @foreach($sectors as $sector)
                                <div>
                                    <input type="checkbox" class="flat-red" name="sectors[]" value="{{ $sector->id }}" {{in_array($sector->id, $project_sectors) ? 'checked' : ''}}>
                                    <label>{{ $sector->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
<!-- iCheck 1.0.1 -->
<script src="{{asset("assets/$theme/plugins/iCheck/icheck.min.js")}}"></script>
<!-- Select2 -->
<script src="{{asset("assets/$theme/bower_components/select2/dist/js/select2.full.min.js")}}"></script>
<!-- CK Editor -->
<script src="{{asset("assets/$theme/bower_components/ckeditor/ckeditor.js")}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset("assets/$theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })

        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
@endsection
