@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Usuarios
        <small>
            @if (request()->is('admin/users/administrators'))
                Administradores
            @elseif (request()->is('admin/users/entrepreneurs'))
                Emprendedores
            @elseif (request()->is('admin/users/investors'))
                Inversores
            @endif
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                        <a href="{{ route('user_create') }}"><i class="fa fa-plus"><span>Nuevo usuario</span></i></a>
                    </div>
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID#</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr {{ $user->blocked ? 'class=blocked' : '' }}>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->lastname }}</td>
                            <td width="12%">
                                <a class="btn btn-actions" href="{{ route('user_show', ['id' => encrypt($user->id)]) }}">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="btn btn-actions" href="{{ route('user_edit', ['id' => encrypt($user->id)]) }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @if ($user->blocked)
                                    <a class="btn btn-actions" href="{{ route('user_block', ['id' => encrypt($user->id), 'value' => 0]) }}">
                                        <i class="fa fa-power-off"></i>
                                    </a>
                                @else
                                    <a class="btn btn-actions" href="{{ route('user_block', ['id' => encrypt($user->id), 'value' => 1]) }}">
                                        <i class="fa fa-check"></i>
                                    </a>
                                @endif
                                <a class="btn btn-actions" href="{{ route('user_delete', ['id' => encrypt($user->id)]) }}">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{asset("assets/$theme/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("assets/$theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script>
        $(function () {
            $('#table').DataTable({
                "bLengthChange" : false,
                "bInfo":false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        })
    </script>
@endsection
