@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Usuarios
        <small>
            {{ $user->name.' '.$user->lastname }}
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Imagen</label>
                        <img class="img-responsive pad" src="{{asset("img/users/".$user->image)}}" alt="Photo">
                    </div>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" class="form-control" disabled value="{{$user->name}}">
                    </div>
                    <div class="form-group">
                        <label>Apellidos</label>
                        <input type="text" class="form-control" disabled value="{{$user->lastname}}">
                    </div>
                    <div class="form-group">
                        <label>DNI</label>
                        <input type="text" class="form-control" disabled value="{{$user->dni}}">
                    </div>
                    <div class="form-group">
                        <label>Cumpleaños:</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" disabled name="birthday" value="{{ date('d/m/Y', strtotime($user->birthday)) }}">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" class="form-control" disabled value="{{$user->phone}}">
                    </div>
                    <div class="form-group">
                        <label>Direccion</label>
                        <input type="text" class="form-control" disabled value="{{$user->address}}">
                    </div>
                    <div class="form-group">
                        <label>Ciudad</label>
                        <input type="text" class="form-control" disabled value="{{$user->city}}">
                    </div>
                    <div class="form-group">
                        <label>País</label>
                        <input type="text" class="form-control" disabled value="{{$user->country}}">
                    </div>
                    <div class="form-group">
                        <label>Código postal</label>
                        <input type="text" class="form-control" disabled value="{{$user->postcode}}">
                    </div>
                    <div class="form-group">
                        <label>Último acceso</label>
                        <input type="text" class="form-control" disabled value="{{$user->last_access}}">
                    </div>
                    <div class="form-group">
                        <label>Observaciones</label>
                        <textarea class="form-control" rows="5" disabled>{{ $user->comments }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Roles</label>
                        <div class="color-palette-set">
                        @foreach($user->roles as $role)
                            @if ($role->name == 'ADMIN')
                                <div class="bg-purple disabled color-palette"><span>ADMIN</span></div>
                            @endif
                            @if ($role->name == 'INVESTOR')
                                <div class="bg-green disabled color-palette"><span>INVERSOR</span></div>
                            @endif
                            @if ($role->name == 'ENTREPRENEUR')
                                <div class="bg-aqua disabled color-palette"><span>EMPRENDEDOR</span></div>
                            @endif
                        @endforeach
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('user_edit', ['id' => encrypt($user->id)]) }}" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    @inject("project", "App\Http\Controllers\Admin\ProjectController")
    @if ($user->hasRole('ENTREPRENEUR'))

        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Proyectos</h3>
                    </div>
                    <!-- form start -->
                    <div class="box-body">
                        @if ($project->numProjectsByUserId($user->id))
                            @foreach($project->projectsByUserId($user->id) as $my_projects)
                            <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="title-project">
                                        <h3 class="widget-user-username"><strong>{{ $my_projects->title }}</strong></h3>
                                    </div>
                                    <div class="widget-user-header bg-black" style="background: url('{{asset("img/projects/".$my_projects->image)}}') center center;">                                </div>
                                    <div class="progress progress-striped active" style="margin-bottom: 0px">
                                        <div class="progress-bar percentage-bar
                                            @if ($project->percentageProjectById($my_projects->id) < 30)
                                            progress-bar-danger
                                            @elseif ($project->percentageProjectById($my_projects->id) >= 30 && ($project->percentageProjectById($my_projects->id) < 60))
                                            progress-bar-yellow
                                            @elseif ($project->percentageProjectById($my_projects->id) >= 60)
                                            progress-bar-success
                                            @endif
                                            " style="width: {{ number_format($project->percentageProjectById($my_projects->id), 2) }}%"><strong>{{ number_format($project->percentageProjectById($my_projects->id), 2) }}%</strong></div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">{{ number_format($my_projects->need_amount, 2) }} &euro;</h5>
                                                    <span class="description-text">NECESITA</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h5 class="description-header">{{ number_format($project->investmentAmountByProjectId($my_projects->id), 2) }} &euro;</h5>
                                                    <span class="description-text">INVERTIDA</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                <div class="description-block">
                                                    <h5 class="description-header">{{ $my_projects->visited }}</h5>
                                                    <span class="description-text">VISITAS</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                    <div class="box-footer">
                                        <div class="row">
                                            <a href="" target="_blank">
                                                <div class="col-sm-6 border-right bg-orange-active">
                                                    <div class="description-block">
                                                        <span class="description-text">VER EN WEB</span>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                            </a>
                                            <!-- /.col -->
                                            <a href="{{ route('project_show', ['id' => encrypt($my_projects->id)]) }}" target="_blank">
                                                <div class="col-sm-6 bg-aqua-active">
                                                    <div class="description-block">
                                                        <span class="description-text">VER PERFIL</span>
                                                    </div>
                                                <!-- /.description-block -->
                                                </div>
                                            </a>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>
                            @endforeach
                        @else
                            <p><strong>Sin proyectos asociados.</strong></p>
                        @endif
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    @endif

    @if ($user->hasRole('INVESTOR'))
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inversión</h3>
                    </div>
                    <!-- form start -->
                    <div class="box-body">
                        @if ($project->numInvestmentByUserId($user->id))
                            @foreach($project->investmentByUserId($user->id) as $my_investment)
                                @php
                                    $my_project = $project->getProjectById($my_investment->project_id)
                                @endphp
                                <div class="col-md-4">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box-widget widget-user">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="title-project">
                                            <h3 class="widget-user-username"><strong>{{ $my_project->title }}</strong></h3>
                                        </div>
                                        <div class="widget-user-header bg-black" style="background: url('{{ asset("img/projects/".$my_project->image) }}') center center;">                                </div>
                                        <div class="progress progress-striped active" style="margin-bottom: 0px">
                                            <div class="progress-bar percentage-bar
                                            @if ($project->percentageProjectById($my_project->id) < 30)
                                                progress-bar-danger
                                            @elseif ($project->percentageProjectById($my_project->id) >= 30 && ($project->percentageProjectById($my_project->id) < 60))
                                                progress-bar-yellow
                                            @elseif ($project->percentageProjectById($my_project->id) >= 60)
                                                progress-bar-success
                                            @endif
                                                " style="width: {{ number_format($project->percentageProjectById($my_project->id), 2) }}%"><strong>{{ number_format($project->percentageProjectById($my_project->id), 2) }}%</strong></div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="row">
                                                <div class="col-sm-12 border-right">
                                                    <div class="description-block">
                                                        <h5 class="description-header">{{ number_format($project->investmentAmountByProjectUserId($my_investment->project_id, $user->id), 2) }} &euro;</h5>
                                                        <span class="description-text">INVERTIDA</span>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            @endforeach
                        @else
                            <p><strong>Sin inversiones asociadas.</strong></p>
                        @endif
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    @endif
@endsection

@section('scripts')

@endsection
