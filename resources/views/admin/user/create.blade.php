@extends("theme.$theme.layout")

@section('styles')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset("assets/$theme/plugins/iCheck/all.css")}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset("assets/$theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}">
@endsection

@section('content-header')
    <h1>
        Usuarios
        <small>
            Crear Usuario
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" action="{{ route('user_store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label>Imagen</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input type="text" class="form-control" name="dni" value="{{ old('dni') }}">
                        </div>
                        <div class="form-group">
                            <label>Cumpleaños:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="birthday" name="birthday" value="{{ old('birthday') }}">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                        </div>
                        <div class="form-group">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                        </div>
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                        </div>
                        <div class="form-group">
                            <label>País</label>
                            <input type="text" class="form-control" name="country" value="{{ old('country') }}">
                        </div>
                        <div class="form-group">
                            <label>Código postal</label>
                            <input type="text" class="form-control" name="postcode" value="{{ old('postcode') }}">
                        </div>
                        <div class="form-group">
                            <label>Observaciones</label>
                            <textarea class="form-control" rows="5" name="comments">{{ old('comments') }}</textarea>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>Roles</label>
                            @foreach($roles as $i => $role)
                                <div>
                                    <input type="checkbox" class="flat-red" name="roles[]" value="{{ $role->id }}" {{is_array(old('roles')) && in_array($role->id, old('roles')) ? 'checked' : ''}}>
                                    @if ($role->name == 'ADMIN')
                                        <label>Administrador</label>
                                    @elseif ($role->name == 'ENTREPRENEUR')
                                        <label>Emprendedor</label>
                                    @elseif ($role->name == 'INVESTOR')
                                        <label>Inversor</label>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
<!-- bootstrap datepicker -->
<script src="{{asset("assets/$theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{asset("assets/$theme/plugins/iCheck/icheck.min.js")}}"></script>
<script>
    $(function () {
        $('#birthday').datepicker({
            autoclose: true
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })
    })
</script>
@endsection
