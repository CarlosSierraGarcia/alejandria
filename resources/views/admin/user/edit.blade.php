@extends("theme.$theme.layout")

@section('styles')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset("assets/$theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}">
@endsection

@section('content-header')
    <h1>
        Usuarios
        <small>
            {{ $user->name.' '.$user->lastname }}
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" action="{{ route('user_update', encrypt($user->id)) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="box-body">
                        <div class="form-group">
                            <label>Imagen</label>
                            <img class="img-responsive pad" src="{{asset("img/users/".$user->image)}}" alt="Photo">
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" name="lastname" value="{{$user->lastname}}">
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input type="text" class="form-control" name="dni" value="{{$user->dni}}">
                        </div>
                        <div class="form-group">
                            <label>Cumpleaños:</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="birthday" name="birthday" value="{{ date('d/m/Y', strtotime($user->birthday)) }}">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone" value="{{$user->phone}}">
                        </div>
                        <div class="form-group">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="address" value="{{$user->address}}">
                        </div>
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" name="city" value="{{$user->city}}">
                        </div>
                        <div class="form-group">
                            <label>País</label>
                            <input type="text" class="form-control" name="country" value="{{$user->country}}">
                        </div>
                        <div class="form-group">
                            <label>Código postal</label>
                            <input type="text" class="form-control" name="postcode" value="{{$user->postcode}}">
                        </div>
                        <div class="form-group">
                            <label>Observaciones</label>
                            <textarea class="form-control" rows="5" name="comments">{{ $user->comments }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Roles</label>
                            <div class="color-palette-set">
                            @foreach($user->roles as $role)
                                @if ($role->name == 'ADMIN')
                                    <div class="bg-purple disabled color-palette"><span>ADMIN</span></div>
                                @endif
                                @if ($role->name == 'INVESTOR')
                                    <div class="bg-green disabled color-palette"><span>INVERSOR</span></div>
                                @endif
                                @if ($role->name == 'ENTREPRENEUR')
                                    <div class="bg-aqua disabled color-palette"><span>EMPRENDEDOR</span></div>
                                @endif
                            @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Acciones</label><br/>
                            @if ($user->blocked)
                                <a class="btn btn-actions" href="{{ route('user_block', ['id' => encrypt($user->id), 'value' => 0]) }}">
                                    <i class="fa fa-unlock"></i>
                                </a>
                            @else
                                <a class="btn btn-actions" href="{{ route('user_block', ['id' => encrypt($user->id), 'value' => 1]) }}">
                                    <i class="fa fa-lock"></i>
                                </a>
                            @endif
                            <a class="btn btn-actions" href="{{ route('user_delete', ['id' => encrypt($user->id)]) }}">
                                <i class="fa fa-remove"></i>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-body -->


                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
    <!-- bootstrap datepicker -->
    <script src="{{asset("assets/$theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js")}}"></script>
    <script>
        $(function () {
            $('#birthday').datepicker({
                autoclose: true
            });
        })
    </script>
@endsection
