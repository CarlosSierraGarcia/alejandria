@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Noticias
        <small>

        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="col-md-6">
                        <a href="{{ route('news_create') }}"><i class="fa fa-plus"><span>Nuevo Noticía</span></i></a>
                    </div>
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID#</th>
                            <th>Título</th>
                            <th>Autor</th>
                            <th>Creado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($news as $news_list)
                            <tr {{ $news_list->visible ? '' : 'class=blocked' }}>
                                <td>{{ $news_list->id }}</td>
                                <td>{{ $news_list->title }}</td>
                                <td>{{ $news_list->author }}</td>
                                <td>{{ date('d/m/Y H:i:s', strtotime($news_list->created_at)) }}</td>
                                <td width="12%">
                                    <a class="btn btn-actions"  href="{{ route('news_show', ['id' => encrypt($news_list->id)]) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a class="btn btn-actions" href="{{ route('news_edit', ['id' => encrypt($news_list->id)]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    @if ($news_list->visible)
                                        <a class="btn btn-actions" href="{{ route('news_block', ['id' => encrypt($news_list->id), 'value' => 0]) }}">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-actions" href="{{ route('news_block', ['id' => encrypt($news_list->id), 'value' => 1]) }}">
                                            <i class="fa fa-power-off"></i>
                                        </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{asset("assets/$theme/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("assets/$theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script>
        $(function () {
            $('#table').DataTable({
                "bLengthChange" : false,
                "bInfo":false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        })
    </script>
@endsection

