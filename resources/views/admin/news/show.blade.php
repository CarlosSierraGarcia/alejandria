@extends("theme.$theme.layout")

@section('styles')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{asset("assets/$theme/plugins/iCheck/all.css")}}">
@endsection

@section('content-header')
    <h1>
        Noticias
        <small>
            {{ $news->title }}
        </small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                        <label>Imagen</label>
                        <img class="img-responsive pad" src="{{asset("img/news/".$news->image)}}" alt="Photo">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Autor</label>
                        <input type="text" class="form-control" disabled value="{{$news->author}}">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Título</label>
                        <input type="text" class="form-control" disabled value="{{$news->title}}">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Descripción</label>
                        <textarea id="editor1" disabled rows="10" cols="80">{{ $news->body }}</textarea>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Fecha de creación</label>
                        <input type="text" class="form-control" disabled value="{{ date('d/m/Y H:i:s', strtotime($news->created_at)) }}">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Categorias</label>
                        @foreach($categories as $category)
                        <div class="form-group">
                            <input type="radio" name="category" class="flat-red" {{ ($category->id == $news->news_category_id) ? 'checked' : ' disabled' }}><label>{{ $category->name }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('news_edit', ['id' => encrypt($news->id)]) }}" class="btn btn-primary">Editar</a>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('scripts')
<!-- iCheck 1.0.1 -->
<script src="{{asset("assets/$theme/plugins/iCheck/icheck.min.js")}}"></script>
<!-- CK Editor -->
<script src="{{asset("assets/$theme/bower_components/ckeditor/ckeditor.js")}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset("assets/$theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
<script>
    $(function () {
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
@endsection
