@extends("theme.$theme.layout")

@section('styles')
@endsection

@section('content-header')
    <h1>
        Inversiones
    </h1>
@endsection

@section('content')
    @inject("project", "App\Http\Controllers\Admin\ProjectController")
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID#</th>
                            <th>Usuario</th>
                            <th>Proyecto</th>
                            <th>Cantidad invertida</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($investments as $investment)
                            @php
                                $my_project = $project->getProjectById($investment->project_id)
                            @endphp
                            <tr>
                                <td>{{ $investment->id }}</td>
                                <td>{{ $investment->owner }}</td>
                                <td>{{ $my_project->title }}</td>
                                <td>{{ number_format($investment->investment_amount, 2) }} &euro;</td>
                                <td>{{ date('d/m/Y H:i:s', strtotime($investment->created_at)) }}</td>
                                <td width="12%">
                                    <a class="btn btn-actions" href="{{ route('user_show', ['id' => encrypt($investment->user_id)]) }}">
                                        <i class="fa fa-user"></i>
                                    </a>
                                    <a class="btn btn-actions" href="{{ route('project_show', ['id' => encrypt($investment->project_id)]) }}">
                                        <i class="fa fa-rocket"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{asset("assets/$theme/bower_components/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{asset("assets/$theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script>
        $(function () {
            $('#table').DataTable({
                "bLengthChange" : false,
                "bInfo":false,
                "language": {
                    "sProcessing":    "Procesando...",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        })
    </script>
@endsection

