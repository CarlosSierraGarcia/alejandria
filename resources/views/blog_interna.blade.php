@extends("theme.front.layout")

@section('content')
    @include('theme.front.search')
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 order-md-last ftco-animate">
                    <h1 class="mb-3 center"><strong>{{ $news->title }}</strong></h1>
                    {!! $news->body !!}
                </div> <!-- .col-md-8 -->

            </div>
            <div class="tag-widget post-tag-container mb-5 mt-5">
                <div class="tagcloud">
                    <p><strong>{{ $news->author }}</strong> {{ date('d M Y', strtotime($news->created_at)) }}</p>
                </div>
            </div>
        </div>
    </section> <!-- .section -->
@endsection
@section('javascript')
@endsection
