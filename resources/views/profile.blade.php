@extends("theme.front.layout")
@section('style')

@endsection

@section('content')
    <section class="ftco-section contact-section">
        <div class="container">
            @include('theme.front.user_menu')
            <div class="row block-9 justify-content-center mb-5">
                <div class="col-md-8 mb-md-5">
                    <h2 class="text-center">Mi Perfil</h2>
                    <form action="{{ route('edit', ['id' => encrypt($user->id)]) }}" class="bg-light p-5 contact-form" method="post" autocomplete="off" enctype="multipart/form-data">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                                {{ $message }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i>Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="form-group center">
                            <img src="{{asset("img/users/".$user->image)}}" alt="Photo">
                        </div>
                        <div class="form-group">
                            <label>Imagen</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" name="lastname" value="{{ $user->lastname }}">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label>DNI</label>
                            <input type="text" class="form-control" name="dni" value="{{ $user->dni }}">
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                        </div>
                        <div class="form-group">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                        </div>
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" name="city" value="{{ $user->city }}">
                        </div>
                        <div class="form-group">
                            <label>País</label>
                            <input type="text" class="form-control" name="country" value="{{ $user->country }}">
                        </div>
                        <div class="form-group">
                            <label>Código postal</label>
                            <input type="text" class="form-control" name="postcode" value="{{ $user->postcode }}">
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Editar" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
@endsection
