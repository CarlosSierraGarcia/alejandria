@extends("theme.front.layout")
@section('style')

@endsection

@section('content')
    <section class="ftco-section contact-section">
        <div class="container">
            @include('theme.front.user_menu')
            @inject("userController", "App\Http\Controllers\UserController")
            <div class="row d-flex mb-5 contact-info justify-content-center">
                <div class="col-md-12">
                    <div class="row mb-5 user-menu">
                        <div class="col-md-12 text-center py-4">
                            <a href="{{ route('my_projects_create') }}" {{ request()->segment(1) == 'profile' && request()->segment(2) == 'my-projects' ? 'class=active' : '' }}>
                                <div class="icon">
                                    <span class="icon-plus"></span>
                                </div>
                                <p><span>Crear Proyecto</span></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                    {{ $message }}
                </div>
            @endif
            @foreach($projects as $project)
                <div>
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th colspan="3" class="center"><a href="" target="_blank" style="color: #007bff">{{ $project->title }}</a></th>
                            {{--<th class="center">Total Necesario: {{ number_format($project->need_amount, 2) }}&euro;</th>
                            <th class="center">Total Invertido: &euro;</th>--}}
                        </tr>
                        </thead>
                            @php $total = 0; @endphp
                            @if (sizeof($userController->investmentOwnerByProjectId($project->id)) > 0)
                                @foreach($userController->investmentOwnerByProjectId($project->id) as $investment)
                                    @php $total += $investment->investment_amount; @endphp
                                    <tr>
                                        <td class="center">{{ $investment->owner }}</td>
                                        <td class="center">{{ $investment->created_at }}</td>
                                        <td class="center">{{ number_format($investment->investment_amount, 2) }}&euro;</td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                <td class="center" colspan="3">Sin inversiones</td>
                            </tr>
                            @endif
                        <tbody>
                        </tbody>
                    </table>
                    <div>
                        <p class="right"><strong>Total Necesario:</strong> {{ number_format($project->need_amount, 2) }}&euro;</p>
                        <p class="right"><strong>Total Invertido:</strong> {{ number_format($total, 2) }}&euro;</p>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
@section('javascript')
@endsection
