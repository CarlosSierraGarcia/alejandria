@extends("theme.front.layout")

@section('content')
    @include('theme.front.search')
    <section class="ftco-section contact-section padding-down">
        <div class="container">
            <div class="row d-flex mb-5 contact-info justify-content-center">
                <div class="col-md-12">
                    <div class="row mb-5 sector">
                        @foreach($sectors as $sector)
                            <div class="col-md-3 text-center py-3 box">
                                <p {{ request()->segment(2) == $sector->slug ? 'class=active' : '' }}><a href="{{ route('projects', ['page' => $sector->slug]) }}"><span>{{ $sector->name }}</span></a></p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section padding-up">
        <div class="container">
            <div class="row d-flex">
                @inject("projectController", "App\Http\Controllers\Admin\ProjectController")
                @foreach ($projects as $project)
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="card categories">
                            <a href="{{ route('project_interna', ['slug' => $project->slug]) }}">
                            <img src="{{ asset("img/projects/".$project->image) }}" class="card-img-top" alt="..." style="height: 145px">
                            <div class="card-body">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{ ($projectController->percentageProjectById($project->id)) > 100 ? 100 : number_format($projectController->percentageProjectById($project->id), 2) }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ number_format($projectController->percentageProjectById($project->id), 2) }}%</div>
                                </div>
                                <h5 class="card-title"><strong>{{ $project->title }}</strong></h5>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            @if ($total > 50)
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
                        <ul>
                            @if ($page > 1)
                            <li><a href="{{ route('projects', ['page' => $page-1]) }}">&lt;</a></li>
                            @endif
                            @for ($i = 1; $i <= ceil($total/50); $i++)
                            <li {{ $i == $page ? 'class="active"' : '' }}><span><a href="{{ route('projects', ['page' => $i]) }}">{{ $i }}</a></span></li>
                            @endfor
                            @if ($page < ceil($total/50))
                            <li><a href="{{ route('projects', ['page' => isset($page) ? 2 : $page+1]) }}">&gt;</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
@endsection
@section('javascript')
@endsection
