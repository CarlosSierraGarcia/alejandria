@extends("theme.front.layout")

@section('content')
    @include('theme.front.search')
    <section class="ftco-section ftco-no-pb">
           <div class="container">
                   <div class="row justify-content-center">
                           <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                                   <span class="subheading">Nuestros Servicios</span>
                                   <h2 class="mb-2">El camino hacia tu nuevo negocio</h2>
                               </div>
                       </div>
                   <div class="row d-flex">
                           <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                                   <div class="media block-6 services d-block text-center">
                                           <div class="icon d-flex justify-content-center align-items-center"><span class="flaticon-piggy-bank"></span></div>
                                           <div class="media-body py-md-4">
                                                   <h3>Asesoramos tu proyecto</h3>
                                                   <p>Nuestros analistas valorarán la viabilidad del proyecto.</p>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                                   <div class="media block-6 services d-block text-center">
                                           <div class="icon d-flex justify-content-center align-items-center"><span class="flaticon-wallet"></span></div>
                                           <div class="media-body py-md-4">
                                                   <h3>Facilidad de inversión</h3>
                                                   <p>Gracias a nuestra red de inversores, tu proyecto se financiara rapidamente.</p>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                                   <div class="media block-6 services d-block text-center">
                                           <div class="icon d-flex justify-content-center align-items-center"><span class="flaticon-file"></span></div>
                                           <div class="media-body py-md-4">
                                                   <h3>Dia a dia con tu proyecto</h3>
                                                   <p>Nos esforzaremos al 200% por potenciar tus ideas del proyecto.</p>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                                   <div class="media block-6 services d-block text-center">
                                           <div class="icon d-flex justify-content-center align-items-center"><span class="flaticon-locked"></span></div>
                                           <div class="media-body py-md-4">
                                                   <h3>Certificación ISO 27001</h3>
                                                   <p>Con nosotros estaras seguro. Protejeremos los datos mas sensibles de tu negocio.</p>
                                               </div>
                                       </div>
                               </div>
                       </div>
               </div>
        </section>

        <section class="ftco-section goto-here">
           <div class="container">
                   <div class="row justify-content-center">
                           <div class="col-md-12 heading-section text-center ftco-animate mb-5">
                                   <span class="subheading">Últimos Proyectos</span>
                                   <h2 class="mb-2">Exclusivos para tí</h2>
                               </div>
                       </div>
                   <div class="row">
                       @inject("projectController", "App\Http\Controllers\Admin\ProjectController")
                       <div class="card-deck">
                       @foreach($projects as $project)
                               <div class="card ">
                                   <a href="">
                                       <img src="{{ asset("img/projects/".$project->image) }}" class="card-img-top" alt="..." style="height: 145px">
                                       <div class="card-body">
                                           <div class="progress">
                                               <div class="progress-bar" role="progressbar" style="width: {{ number_format($projectController->percentageProjectById($project->id), 2) }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ number_format($projectController->percentageProjectById($project->id), 2) }}%</div>
                                           </div>
                                           <h5 class="card-title"><strong>{{ $project->title }}</strong></h5>
                                           <p class="card-text">{!! $project->description !!}</p>
                                       </div>
                                   </a>
                               </div>
                           @endforeach
                       </div>
                   </div>
           </div>
        </section>

    <section class="ftco-section ftco-degree-bg services-section img mx-md-5" style="background-image: url({{ asset('assets/front/images/como-funciona.jpg') }});">
           <div class="overlay"></div>
           <div class="container">
                   <div class="row justify-content-start mb-5">
                           <div class="col-md-6 text-center heading-section heading-section-white ftco-animate">
                                   <span class="subheading">¿Cómo funciona?</span>
                               </div>
                       </div>
                   <div class="row">
                           <div class="col-md-6">
                                   <div class="row">
                                           <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                                                   <div class="media block-6 services services-2">
                                                           <div class="media-body py-md-4 text-center">
                                                                   <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>01</span></div>
                                                                   <h3>Emprendedores registran un proyecto en sociosinversores.com</h3>
                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                                                   <div class="media block-6 services services-2">
                                                           <div class="media-body py-md-4 text-center">
                                                                   <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>02</span></div>
                                                                   <h3>Nuestros analistas valorarán la viabilidad del proyecto</h3>

                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                                                   <div class="media block-6 services services-2">
                                                           <div class="media-body py-md-4 text-center">
                                                                   <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>03</span></div>
                                                                   <h3>Los inversores se registran e invierten en los proyectos</h3>
                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                                                   <div class="media block-6 services services-2">
                                                           <div class="media-body py-md-4 text-center">
                                                                   <div class="icon mb-3 d-flex align-items-center justify-content-center"><span>04</span></div>
                                                                   <h3>El proyecto se financia con éxito al poco tiempo</h3>
                                                               </div>
                                                       </div>
                                               </div>
                                       </div>
                               </div>
                       </div>
               </div>
        </section>

    <section class="ftco-counter img" id="section-counter">
           <div class="container">
               <div class="row justify-content-start mb-5">
                   <div class="col-md-6 text-center heading-section heading-section-white ftco-animate">
                       <span class="subheading">Nuestros Resultados</span>
                   </div>
               </div>
                   <div class="row">
                           <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                                   <div class="block-18 py-4 mb-4">
                                           <div class="text text-border d-flex align-items-center">
                                                   <strong class="number" data-number="305">0</strong>
                                                   <span>Total <br>Emprendedores</span>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                                   <div class="block-18 py-4 mb-4">
                                           <div class="text text-border d-flex align-items-center">
                                                   <strong class="number" data-number="1090">0</strong>
                                                   <span>Total <br>Inversores</span>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                                   <div class="block-18 py-4 mb-4">
                                           <div class="text text-border d-flex align-items-center">
                                                   <strong class="number" data-number="209">0</strong>
                                                   <span>Total <br>Proyectos</span>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate">
                                   <div class="block-18 py-4 mb-4">
                                           <div class="text d-flex align-items-center">
                                                   <strong class="number" data-number="67">0</strong>
                                                   <span>Total <br>Financiados</span>
                                               </div>
                                       </div>
                               </div>
                       </div>
               </div>
        </section>

    <section class="ftco-section testimony-section">
           <div class="container">
                   <div class="row justify-content-center mb-5">
                           <div class="col-md-7 text-center heading-section ftco-animate">
                                   <span class="subheading">¿Que dicen de nosotros?</span>
                                   <h2 class="mb-3">Clientes Felices</h2>
                               </div>
                       </div>
                   <div class="row ftco-animate">
                           <div class="col-md-12">
                                   <div class="carousel-testimony owl-carousel ftco-owl">
                                           <div class="item">
                                                   <div class="testimony-wrap py-4">
                                                           <div class="text">
                                                                   <p class="mb-4">Sin duda alguna lo mejor del sector.Enhorabuena por el buen trabajo y la gran profesionalidad. Un gusto.</p>
                                                                   <div class="d-flex align-items-center">
                                                                           <div class="user-img" style="background-image: url({{ asset('assets/front/images/person_1.jpg') }})"></div>
                                                                           <div class="pl-3">
                                                                                   <p class="name">Juan Pedrosa</p>
                                                                               </div>
                                                                       </div>
                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="item">
                                                   <div class="testimony-wrap py-4">
                                                           <div class="text">
                                                                   <p class="mb-4">Gran empresa, con gente seria, muy profesional profesional y cualificada. Enhorabuena, Seguir asi chicos.</p>
                                                                   <div class="d-flex align-items-center">
                                                                           <div class="user-img" style="background-image: url({{ asset('assets/front/images/person_2.jpg') }})"></div>
                                                                           <div class="pl-3">
                                                                                   <p class="name">Marcos Scott</p>
                                                                               </div>
                                                                       </div>
                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="item">
                                                   <div class="testimony-wrap py-4">
                                                           <div class="text">
                                                                   <p class="mb-4">Empece mi nuevo proyecto con ellos y en 3 meses obtuve toda la financiación necesaria. Muchas Gracias!</p>
                                                                   <div class="d-flex align-items-center">
                                                                           <div class="user-img" style="background-image: url({{ asset('assets/front/images/person_3.jpg') }})"></div>
                                                                           <div class="pl-3">
                                                                                   <p class="name">Pablo Villa</p>
                                                                               </div>
                                                                       </div>
                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="item">
                                                   <div class="testimony-wrap py-4">
                                                           <div class="text">
                                                                   <p class="mb-4">Todo muy rápido y sencillo. El nivel de esta empresa es muy alto y te ofrece soluciones para todo.</p>
                                                                   <div class="d-flex align-items-center">
                                                                           <div class="user-img" style="background-image: url({{ asset('assets/front/images/person_1.jpg') }})"></div>
                                                                           <div class="pl-3">
                                                                                   <p class="name">Alberto García</p>
                                                                               </div>
                                                                       </div>
                                                               </div>
                                                       </div>
                                               </div>
                                           <div class="item">
                                                   <div class="testimony-wrap py-4">
                                                           <div class="text">
                                                                   <p class="mb-4">Son muchas las empresas que intentaron impulsar mi negocio, pero esta es la mejor de todas. Muy profesionales!</p>
                                                                   <div class="d-flex align-items-center">
                                                                           <div class="user-img" style="background-image: url({{ asset('assets/front/images/person_2.jpg') }})"></div>
                                                                           <div class="pl-3">
                                                                                   <p class="name">Carlos Perez</p>
                                                                               </div>
                                                                       </div>
                                                               </div>
                                                       </div>
                                               </div>
                                       </div>
                               </div>
                       </div>
               </div>
        </section>

    <section class="ftco-section ftco-agent ftco-no-pt">
           <div class="container">
                   <div class="row justify-content-center pb-5">
                           <div class="col-md-12 heading-section text-center ftco-animate">
                                   <span class="subheading">Nuestro Equipo</span>
                                   <h2 class="mb-4">Unete a nosotros</h2>
                               </div>
                       </div>
                   <div class="row">
                           <div class="col-md-3 ftco-animate">
                                   <div class="agent">
                                           <div class="img">
                                                   <img src="{{ asset('assets/front/images/team-1.jpg') }}" class="img-fluid" alt="Colorlib Template">
                                               </div>
                                           <div class="desc">
                                                   <h3>Carlos Sierra García</h3>
                                                   <p class="h-info"><span class="location">Director General y Fundador</span></p>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-3 ftco-animate">
                                   <div class="agent">
                                           <div class="img">
                                                   <img src="{{ asset('assets/front/images/team-2.jpg') }}" class="img-fluid" alt="Colorlib Template">
                                               </div>
                                           <div class="desc">
                                                   <h3>Leandro Sierra</h3>
                                                   <p class="h-info"><span class="location">Responsable de Operaciones</span></p>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-3 ftco-animate">
                                   <div class="agent">
                                           <div class="img">
                                                   <img src="{{ asset('assets/front/images/team-3.jpg') }}" class="img-fluid" alt="Colorlib Template">
                                               </div>
                                           <div class="desc">
                                                   <h3>Victoria Marín</h3>
                                                   <p class="h-info"><span class="location">Responsable de Diseño Gráfico y UX/UI</span></p>
                                               </div>
                                       </div>
                               </div>
                           <div class="col-md-3 ftco-animate">
                                   <div class="agent">
                                           <div class="img">
                                                   <img src="{{ asset('assets/front/images/team-4.jpg') }}" class="img-fluid" alt="Colorlib Template">
                                               </div>
                                           <div class="desc">
                                                   <h3>Francisco Sanz</h3>
                                                   <p class="h-info"><span class="location">Responsable de Finanzas</span></p>
                                               </div>
                                       </div>
                               </div>
                       </div>
               </div>
        </section>
    <section class="ftco-section ftco-no-pt">
           <div class="container">
                   <div class="row justify-content-center mb-5">
                           <div class="col-md-7 heading-section text-center ftco-animate">
                                   <span class="subheading">Blog</span>
                                   <h2>Últimas Noticias</h2>
                               </div>
                       </div>
                   <div class="row d-flex">
                           @foreach ($blog as $news)
                           <div class="col-md-3 d-flex ftco-animate">
                                   <div class="blog-entry justify-content-end">
                                           <div class="text">
                                                   <h3 class="heading"><a href="#">{{ $news->title }}</a></h3>
                                                   <div class="meta mb-3">
                                                           <div><a href="#">{{ date('d/m/Y', strtotime($news->created_at)) }}</a></div>
                                                           <div><a href="#">{{ $news->author }}</a></div>
                                                       </div>
                                                   <a href="blog-single.html" class="block-20 img" style="background-image: url('{{asset("img/news/".$news->image)}}');"></a>
                                               </div>
                                       </div>
                               </div>
                           @endforeach
                       </div>
               </div>
        </section>
    @endsection
@section('javascript')
<script>
    $(function () {
        $('.progress-bar'+{{ $project->id }}).css('width', {{ number_format($projectController->percentageProjectById($project->id), 2) }}+'%');
    })
</script>
@endsection
