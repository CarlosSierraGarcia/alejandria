@extends("theme.front.layout")

@section('content')
    @include('theme.front.search')
    <section class="ftco-section contact-section padding-down">
        <div class="container">
            <div class="row d-flex mb-5 contact-info justify-content-center">
                <div class="col-md-12">
                    <div class="row mb-12 sector">
                        @foreach($categories as $category)
                            <div class="col-md-4 text-center py-3 box">
                                <p {{ request()->segment(2) == $category->slug ? 'class=active' : '' }}><a href="{{ route('blog_category', ['slug' => $category->slug]) }}"><span>{{ $category->name }}</span></a></p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section padding-up">
        <div class="container">
            <div class="row d-flex">
                @foreach ($blog as $news)
                <div class="col-md-3 d-flex ftco-animate">
                    <div class="blog-entry justify-content-end">
                        <div class="text">
                            <h3 class="heading"><a href="{{ route('blog_interna', ['slug' => $news->slug]) }}">{{ $news->title }}</a></h3>
                            <div class="meta mb-3">
                                <div><a href="{{ route('blog_interna', ['slug' => $news->slug]) }}">{{ date('d M Y', strtotime($news->created_at)) }}</a></div>
                                <div><a href="{{ route('blog_interna', ['slug' => $news->slug]) }}">{{ $news->author }}</a></div>
                            </div>
                            <a href="{{ route('blog_interna', ['slug' => $news->slug]) }}" class="block-20 img" style="background-image: url({{asset("img/news/".$news->image)}});">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
@section('javascript')
@endsection
