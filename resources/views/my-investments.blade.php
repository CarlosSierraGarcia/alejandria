@extends("theme.front.layout")
@section('style')

@endsection

@section('content')
    <section class="ftco-section contact-section">
        <div class="container">
            @include('theme.front.user_menu')
            <table id="table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="center">Proyecto</th>
                    <th class="center">Nombre</th>
                    <th class="center">Fecha</th>
                    <th class="center">Cantidad invertida</th>
                </tr>
                </thead>
                <tbody>
                    @php $total = 0; @endphp
                    @foreach($investments as $investment)
                        @php $total += $investment->investment_amount; @endphp
                        <tr>
                            <td class="center"><a href="" target="_blank" style="color: #007bff">{{ $investment->title }}</a></td>
                            <td class="center">{{ $investment->owner }}</td>
                            <td class="center">{{ $investment->created_at }}</td>
                            <td class="center">{{ number_format($investment->investment_amount, 2) }}&euro;</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br/>
            <div>
                <p class="right"><strong>Total Invertido:</strong> {{ number_format($total, 2) }}&euro;</p>
            </div>

        </div>
    </section>
@endsection
@section('javascript')
@endsection
