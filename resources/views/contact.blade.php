@extends("theme.front.layout")

@section('content')
    @include('theme.front.search')
    <section class="ftco-section contact-section">
        <div class="container">
            <div class="row d-flex mb-5 contact-info justify-content-center">
                <div class="col-md-8">
                    <div class="row mb-5">
                        <div class="col-md-4 text-center py-4">
                            <div class="icon">
                                <span class="icon-map-o"></span>
                            </div>
                            <p><span>Dirección:</span> Paseo de la castellana 33, Madrid, España</p>
                        </div>
                        <div class="col-md-4 text-center border-height py-4">
                            <div class="icon">
                                <span class="icon-mobile-phone"></span>
                            </div>
                            <p><span>Teléfono:</span> <a href="tel://689075043">689 07 50 43</a></p>
                        </div>
                        <div class="col-md-4 text-center py-4">
                            <div class="icon">
                                <span class="icon-envelope-o"></span>
                            </div>
                            <p><span>E-mail:</span> <a href="mailto:carlos.sierra.garcia@gmail.com">carlos.sierra.garcia@gmail.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row block-9 justify-content-center mb-5">
                <div class="col-md-8 mb-md-5">
                    <h2 class="text-center">Si tienes alguna pregunta <br>ponte en contacto con nosotros</h2>
                    <form action="{{ route('contact_send') }}" class="bg-light p-5 contact-form" method="post" autocomplete="off">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Aviso!</h4>
                                {{ $message }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i>Error!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name"  placeholder="Tu nombre">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email"  placeholder="Tu e-mail">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject"  placeholder="Asunto">
                        </div>
                        <div class="form-group">
                            <textarea cols="30" rows="7" name="message" class="form-control" placeholder="Mensaje"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Enviar" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascript')
@endsection
