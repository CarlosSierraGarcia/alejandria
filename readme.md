<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Alejandria

## Installation

- Go to C:\Windows\System32\drivers\etc\hosts

```
127.0.0.1         alejandria.com
```

- Go to C:\xampp\apache\conf\extra\httpd-vhosts.conf
```
<VirtualHost *:80>
    DocumentRoot "C:/xampp/htdocs"
    ServerName localhost
</VirtualHost>

<VirtualHost *:80>
    ServerAdmin carlos.sierra@marketinet.com
    DocumentRoot "C:/xampp/htdocs/alejandria/public/"
    ServerName alejandria.com
    ErrorLog "logs/dummy-host2.example.com-error.log"
    CustomLog "logs/dummy-host2.example.com-access.log" common
</VirtualHost>
```

- Run composer update to install vendors.
- Copy the file .env.example, rename the file at .env and config the database.
- Run the next commands:
```
php artisan key:generate                 --> To use Laravel's encrypter.
php artisan migrate                      --> To install database.
php artisan db:seed                      --> To install randon data.
php artisan vendor:publish --tag=lang    --> To install Spanish language
```
## Documentation

- [Laravel](https://laravel.com/docs/5.8)
- [Videotutorial](https://www.youtube.com/channel/UCzUaa-ts60lIXwzIQYQngRQ)
